import React from 'react';
import { Redirect } from "react-router-dom";
import { Route } from 'react-router-dom';

import { isUserAuthenticated, getLoggedInUser } from './helpers/authUtils';


const Dashboard = React.lazy(() => import('./pages/dashboards/'));
const Chart = React.lazy(() => import('./pages/dashboards/chart'));
const Sets = React.lazy(() => import('./pages/dashboards/sets'));
const Issues = React.lazy(() => import('./pages/dashboards/issues'));
const Coupons = React.lazy(() => import('./pages/dashboards/coupons'));

const Approved = React.lazy(() => import('./pages/insurances/Approved'));
const Insured = React.lazy(() => import('./pages/insurances/Insured'));
const Cancelled = React.lazy(() => import('./pages/insurances/Cancelled'));
const CancelRequest = React.lazy(() => import('./pages/insurances/Cancel_request'));
const Exceeded = React.lazy(() => import('./pages/insurances/Exceeded'));
const Ended = React.lazy(() => import('./pages/insurances/Ended'));
const Claims = React.lazy(() => import('./pages/insurances/Claims'));

const Login = React.lazy(() => import('./pages/auth/Login'));
const Logout = React.lazy(() => import('./pages/auth/Logout'));
const ForgetPassword = React.lazy(() => import('./pages/account/ForgetPassword'));
const Register = React.lazy(() => import('./pages/account/Register'));
const ConfirmAccount = React.lazy(() => import('./pages/account/Confirm'));



const PrivateRoute = ({ component: Component, roles, ...rest }) => (
  <Route {...rest} render={props => {
    const isAuthTokenValid = isUserAuthenticated();
    if (!isAuthTokenValid) {

      return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    }

    const loggedInUser = getLoggedInUser();

    if (roles && roles.indexOf(loggedInUser.role) === -1) {

      return <Redirect to={{ pathname: '/' }} />
    }


    return <Component {...props} />
  }} />
)

const routes = [

  { path: '/login', name: 'Login', component: Login, route: Route },
  { path: '/logout', name: 'Logout', component: Logout, route: Route },
  { path: '/forget-password', name: 'Forget Password', component: ForgetPassword, route: Route },
  { path: '/register', name: 'Register', component: Register, route: Route },
  { path: '/confirm', name: 'Confirm', component: ConfirmAccount, route: Route },

  { path: '/dashboard', name: 'Dashboard', component: Dashboard, route: PrivateRoute, roles: ['Admin'] },
  { path: '/chart', name: 'Chart', component: Chart, route: PrivateRoute, roles: ['Admin'] },
  { path: '/sets', name: 'Sets', component: Sets, route: PrivateRoute, roles: ['Admin'] },
  { path: '/issues', name: 'Issues', component: Issues, route: PrivateRoute, roles: ['Admin'] },
  { path: '/coupons', name: 'Coupons', component: Coupons, route: PrivateRoute, roles: ['Admin'] },

  { path: '/approved', name: 'Approved', component: Approved, route: PrivateRoute, roles: ['Admin'] },
  { path: '/insured', name: 'Insured', component: Insured, route: PrivateRoute, roles: ['Admin'] },
  { path: '/cancelled', name: 'Cancelled', component: Cancelled, route: PrivateRoute, roles: ['Admin'] },
  { path: '/cancel_request', name: 'CancelRequest', component: CancelRequest, route: PrivateRoute, roles: ['Admin'] },
  { path: '/exceeded', name: 'Exceeded', component: Exceeded, route: PrivateRoute, roles: ['Admin'] },
  { path: '/ended', name: 'Ended', component: Ended, route: PrivateRoute, roles: ['Admin'] },
  { path: '/claims', name: 'Claims', component: Claims, route: PrivateRoute, roles: ['Admin'] },

  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/dashboard" />,
    route: PrivateRoute
  },{
    path: "/",
    exact: true,
    component: () => <Redirect to="/sets" />,
    route: PrivateRoute
  },{
    path: "/",
    exact: true,
    component: () => <Redirect to="/issues" />,
    route: PrivateRoute
  },{
    path: "/",
    exact: true,
    component: () => <Redirect to="/coupons" />,
    route: PrivateRoute
  },{
    path: "/",
    exact: true,
    component: () => <Redirect to="/chart" />,
    route: PrivateRoute
  },
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/approved" />,
    route: PrivateRoute
  },
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/insured" />,
    route: PrivateRoute
  },
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/cancelled" />,
    route: PrivateRoute
  },
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/cancel_request" />,
    route: PrivateRoute
  },
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/exceeded" />,
    route: PrivateRoute
  },
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/ended" />,
    route: PrivateRoute
  },
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/claims" />,
    route: PrivateRoute
  },
  
]

export { routes, PrivateRoute };