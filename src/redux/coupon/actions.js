import {
    GET_COUPON_STARTED,
    GET_COUPON_SUCCESS,
    GET_COUPON_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getCouponStarted = () => {
    return {
        type: GET_COUPON_STARTED
    }
}

export const getCouponSuccess = (payload) => {
    return {
        type: GET_COUPON_SUCCESS,
        payload
    }
}

export const getCouponFailed = (payload) => {
    return {
        type: GET_COUPON_FAILED,
        payload
    }
}
