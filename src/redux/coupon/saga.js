import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_COUPON_STARTED
} from '../../constants/actionTypes';

import {
    getCouponSuccess,
    getCouponFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getCoupon(action, pageNumber) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/coupons?page='+pageNumber);
        yield put(getCouponSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getCouponFailed(message));
    }
}

export function* watchCoupon() {
    yield takeEvery(GET_COUPON_STARTED, getCoupon);
}

function* couponSaga() {
    yield all([
        fork(watchCoupon),
    ])
}

export default couponSaga;