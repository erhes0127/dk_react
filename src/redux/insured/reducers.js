import {
    GET_INSURED_STARTED,
    GET_INSURED_SUCCESS,
    GET_INSURED_FAILED,
} from '../../constants/actionTypes';


const initialState = {
    error: undefined,
    loading: false,
    data: [],
    first_page_url: "",
    from: 1,
    last_page: 1,
    last_page_url: "",
    next_page_url: "",
    path: "",
    per_page: 1,
    prev_page_url: "",
    to: 0,
    total: 1,
    current_page: 1,
}

export default (state = initialState, action) => {
    console.log(action)
    switch (action.type) {
        case GET_INSURED_STARTED: 
            return {
                ...state,
                error: undefined,
                loading: true,
            }
        case GET_INSURED_SUCCESS:
            return {
                ...state,
                loading: false,
                ...action.payload,
            }
        case GET_INSURED_FAILED:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        default: return state;
    }
}