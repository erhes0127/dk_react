import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_INSURED_STARTED
} from '../../constants/actionTypes';

import {
    getInsuredSuccess,
    getInsuredFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getInsured(action) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/insurance');
        yield put(getInsuredSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getInsuredFailed(message));
    }
}

export function* watchInsured() {
    yield takeEvery(GET_INSURED_STARTED, getInsured);
}

function* insuredSaga() {
    yield all([
        fork(watchInsured),
    ])
}

export default insuredSaga;