import {
    GET_INSURED_STARTED,
    GET_INSURED_SUCCESS,
    GET_INSURED_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getInsuredStarted = () => {
    return {
        type: GET_INSURED_STARTED
    }
}

export const getInsuredSuccess = (payload) => {
    return {
        type: GET_INSURED_SUCCESS,
        payload
    }
}

export const getInsuredFailed = (payload) => {
    return {
        type: GET_INSURED_FAILED,
        payload
    }
}
