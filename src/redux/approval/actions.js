import {
    GET_APPROVAL_STARTED,
    GET_APPROVAL_SUCCESS,
    GET_APPROVAL_FAILED,
    DELETE_APPROVAL_STARTED,
    DELETE_APPROVAL_SUCCESS,
    DELETE_APPROVAL_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getApprovalStarted = () => {
    return {
        type: GET_APPROVAL_STARTED
    }
}

export const getApprovalSuccess = (payload) => {
    return {
        type: GET_APPROVAL_SUCCESS,
        payload
    }
}

export const getApprovalFailed = (payload) => {
    return {
        type: GET_APPROVAL_FAILED,
        payload
    }
}

export const deleteApprovalStarted = () => {
    return {
        type: DELETE_APPROVAL_STARTED
    }
}

export const deleteApprovalSuccess = (payload) => {
    return {
        type: DELETE_APPROVAL_SUCCESS,
        payload
    }
}

export const deleteApprovalFailed = (payload) => {
    return {
        type: DELETE_APPROVAL_FAILED,
        payload
    }
}