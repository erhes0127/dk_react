import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_APPROVAL_STARTED,
    DELETE_APPROVAL_STARTED,
} from '../../constants/actionTypes';

import {
    getApprovalSuccess,
    getApprovalFailed,
    deleteApprovalSuccess,
    deleteApprovalFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getApproval(action) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/approvals');
        yield put(getApprovalSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getApprovalFailed(message));
    }
}

function* deleteApproval(action) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/{id}/cancel');
        yield put(deleteApprovalSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(deleteApprovalFailed(message));
    }
}

export function* watchApproval() {
    yield takeEvery(GET_APPROVAL_STARTED, getApproval);
}
export function* watchDeleteApproval() {
    yield takeEvery(DELETE_APPROVAL_STARTED, deleteApproval);
}

function* approvalSaga() {
    yield all([
        fork(watchApproval),
        fork(watchDeleteApproval),
    ])
}

export default approvalSaga;