import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_EXCEED_STARTED
} from '../../constants/actionTypes';

import {
    getExceedSuccess,
    getExceedFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getExceed(action) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/exceed');
        yield put(getExceedSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getExceedFailed(message));
    }
}

export function* watchExceed() {
    yield takeEvery(GET_EXCEED_STARTED, getExceed);
}

function* exceedSaga() {
    yield all([
        fork(watchExceed),
    ])
}

export default exceedSaga;