import {
    GET_EXCEED_STARTED,
    GET_EXCEED_SUCCESS,
    GET_EXCEED_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getExceedStarted = () => {
    return {
        type: GET_EXCEED_STARTED
    }
}

export const getExceedSuccess = (payload) => {
    return {
        type: GET_EXCEED_SUCCESS,
        payload
    }
}

export const getExceedFailed = (payload) => {
    return {
        type: GET_EXCEED_FAILED,
        payload
    }
}
