import {
    GET_CANCEL_STARTED,
    GET_CANCEL_SUCCESS,
    GET_CANCEL_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getCancelStarted = () => {
    return {
        type: GET_CANCEL_STARTED
    }
}

export const getCancelSuccess = (payload) => {
    return {
        type: GET_CANCEL_SUCCESS,
        payload
    }
}

export const getCancelFailed = (payload) => {
    return {
        type: GET_CANCEL_FAILED,
        payload
    }
}
