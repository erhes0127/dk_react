import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_CANCEL_STARTED
} from '../../constants/actionTypes';

import {
    getCancelSuccess,
    getCancelFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getCancel(action) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/cancel');
        yield put(getCancelSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getCancelFailed(message));
    }
}

export function* watchCancel() {
    yield takeEvery(GET_CANCEL_STARTED, getCancel);
}

function* cancelSaga() {
    yield all([
        fork(watchCancel),
    ])
}

export default cancelSaga;