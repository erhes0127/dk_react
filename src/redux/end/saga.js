import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_END_STARTED
} from '../../constants/actionTypes';

import {
    getEndSuccess,
    getEndFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getEnd(action) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/end');
        yield put(getEndSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getEndFailed(message));
    }
}

export function* watchEnd() {
    yield takeEvery(GET_END_STARTED, getEnd);
}

function* endSaga() {
    yield all([
        fork(watchEnd),
    ])
}

export default endSaga;