import {
    GET_END_STARTED,
    GET_END_SUCCESS,
    GET_END_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getEndStarted = () => {
    return {
        type: GET_END_STARTED
    }
}

export const getEndSuccess = (payload) => {
    return {
        type: GET_END_SUCCESS,
        payload
    }
}

export const getEndFailed = (payload) => {
    return {
        type: GET_END_FAILED,
        payload
    }
}
