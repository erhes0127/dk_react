import {
    GET_END_STARTED,
    GET_END_SUCCESS,
    GET_END_FAILED,
} from '../../constants/actionTypes';


const initialState = {
    error: undefined,
    loading: false,
    data: [],
    first_page_url: "",
    from: 1,
    last_page: 1,
    last_page_url: "",
    next_page_url: null,
    path: "",
    per_page: 0,
    prev_page_url: null,
    to: 0,
    total: 0,
    current_page: 1,
}

export default (state = initialState, action) => {
    // console.log(action)
    switch (action.type) {
        case GET_END_STARTED: 
            return {
                ...state,
                error: undefined,
                loading: true,
            }
        case GET_END_SUCCESS:
            return {
                ...state,
                loading: false,
                ...action.payload,
            }
        case GET_END_FAILED:
            return {
                ...state,
                loading: false,
                error: action.payload,
            }
        default: return state;
    }
}