import { combineReducers } from 'redux';
import Auth from './auth/reducers';
import Approval from './approval/reducers';
import Insured from './insured/reducers';
import Cancel from './cancel/reducers';
import CancelRequest from './cancel_request/reducers';
import Exceed from './exceed/reducers';
import End from './end/reducers';
import Set from './set/reducers';
import Issue from './issue/reducers';
import Coupon from './coupon/reducers';

export default combineReducers({
    Auth,
    Approval,
    Insured,
    Cancel,
    CancelRequest,
    Exceed,
    End,
    Set,
    Issue,
    Coupon,
});