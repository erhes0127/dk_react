import {
    GET_ISSUE_STARTED,
    GET_ISSUE_SUCCESS,
    GET_ISSUE_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getIssueStarted = () => {
    return {
        type: GET_ISSUE_STARTED
    }
}

export const getIssueSuccess = (payload) => {
    return {
        type: GET_ISSUE_SUCCESS,
        payload
    }
}

export const getIssueFailed = (payload) => {
    return {
        type: GET_ISSUE_FAILED,
        payload
    }
}
