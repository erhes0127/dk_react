import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_ISSUE_STARTED
} from '../../constants/actionTypes';

import {
    getIssueSuccess,
    getIssueFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getIssue(action, pageNumber) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/issues?page='+pageNumber);
        yield put(getIssueSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getIssueFailed(message));
    }
}

export function* watchIssue() {
    yield takeEvery(GET_ISSUE_STARTED, getIssue);
}

function* issueSaga() {
    yield all([
        fork(watchIssue),
    ])
}

export default issueSaga;