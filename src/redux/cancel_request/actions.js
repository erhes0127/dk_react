import {
    GET_CANCEL_REQUEST_STARTED,
    GET_CANCEL_REQUEST_SUCCESS,
    GET_CANCEL_REQUEST_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getCancelRequestStarted = () => {
    return {
        type: GET_CANCEL_REQUEST_STARTED
    }
}

export const getCancelRequestSuccess = (payload) => {
    return {
        type: GET_CANCEL_REQUEST_SUCCESS,
        payload
    }
}

export const getCancelRequestFailed = (payload) => {
    return {
        type: GET_CANCEL_REQUEST_FAILED,
        payload
    }
}
