import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_CANCEL_REQUEST_STARTED
} from '../../constants/actionTypes';

import {
    getCancelRequestSuccess,
    getCancelRequestFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getCancelRequest(action) {
    try {
        const {data} = yield call(getApi, 'http://localhost:8000/api/cancel_request');
        yield put(getCancelRequestSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getCancelRequestFailed(message));
    }
}

export function* watchCancelRequest() {
    yield takeEvery(GET_CANCEL_REQUEST_STARTED, getCancelRequest);
}

function* cancelRequestSaga() {
    yield all([
        fork(watchCancelRequest),
    ])
}

export default cancelRequestSaga;