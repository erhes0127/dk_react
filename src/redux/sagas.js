import { all } from 'redux-saga/effects';
import authSaga from './auth/saga';
import approvalSaga from './approval/saga';
import insuredSaga from './insured/saga';
import cancelSaga from './cancel/saga';
import cancelRequestSaga from './cancel_request/saga';
import exceedSaga from './exceed/saga';
import endSaga from './end/saga';
import setSaga from './set/saga';
import issueSaga from './issue/saga';
import couponSaga from './coupon/saga';

export default function* rootSaga(getState) {
    yield all([
        authSaga(),
        approvalSaga(),
        insuredSaga(),
        cancelSaga(),
        cancelRequestSaga(),
        exceedSaga(),
        endSaga(),
        setSaga(),
        issueSaga(),
        couponSaga(),
    ]);
}
