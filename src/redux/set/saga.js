import { Cookies } from "react-cookie";
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios'

import {
    GET_SET_STARTED
} from '../../constants/actionTypes';

import {
    getSetSuccess,
    getSetFailed,
} from './actions'

const getApi = (url, params = {}) => {
    return axios.get(url, params);
}

function* getSet(action) {
    try {
        console.log(data);
        const {data} = yield call(getApi, 'http://localhost:8000/api/sets');
        yield put(getSetSuccess(data))
    } catch (error) {
        let message;
        switch (error.status) {
            case 500: message = 'Internal Server Error'; break;
            case 401: message = 'Invalid credentials'; break;
            default: message = error;
        }
        yield put(getSetFailed(message));
    }
}

export function* watchSet() {
    yield takeEvery(GET_SET_STARTED, getSet);
}

function* setSaga() {
    yield all([
        fork(watchSet),
    ])
}

export default setSaga;