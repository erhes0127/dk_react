import {
    GET_SET_STARTED,
    GET_SET_SUCCESS,
    GET_SET_FAILED,
} from '../../constants/actionTypes';
import { put } from 'redux-saga/effects';
import axios from 'axios';

export const getSetStarted = () => {
    return {
        type: GET_SET_STARTED
    }
}

export const getSetSuccess = (payload) => {
    return {
        type: GET_SET_SUCCESS,
        payload
    }
}

export const getSetFailed = (payload) => {
    return {
        type: GET_SET_FAILED,
        payload
    }
}
