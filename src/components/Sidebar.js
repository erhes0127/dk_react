import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import MetisMenu from 'metismenujs/dist/metismenujs';
import axios from 'axios';
import { bindActionCreators } from 'redux';
import { getApprovalStarted } from '../redux/approval/actions';
import { getInsuredStarted } from '../redux/insured/actions';
import { getCancelStarted } from '../redux/cancel/actions';
import { getCancelRequestStarted } from '../redux/cancel_request/actions';
import { getExceedStarted } from '../redux/exceed/actions';
import { getEndStarted } from '../redux/end/actions';
import { getSetStarted } from '../redux/set/actions';
import { getIssueStarted } from '../redux/issue/actions';
import { getCouponStarted } from '../redux/coupon/actions';

const SideNavContent = (props) => {
    const sideStyle = {
        fontSize: "13px"
    }

    return <React.Fragment>

        <div id="sidebar-menu">
            <ul className="metismenu" id="side-menu">
                <li className="menu-title">НЭГДСЭН САМБАР</li>
                <li>
                    <Link to="/chart" className="waves-effect has-dropdown" aria-expanded="false">
                        <i class="far fa-chart-bar"></i>
                        <span style={sideStyle}> График үзүүлэлт </span>
                    </Link>
                </li>
                <li>
                    <Link to="/sets" className="waves-effect has-dropdown" aria-expanded="false">
                        <i className="fe-folder-plus"></i>
                        <span style={sideStyle}> Манай багц</span>
                    </Link>
                </li>
                <li>
                    {/* <Link to="/" className="waves-effect has-dropdown" aria-expanded="true">
                        <i className="fe-folder-plus"></i>
                        <span className="badge badge-success badge-pill float-right">6</span>
                        <span> Даатгал </span>
                    </Link> */}
                    <hr/>
                    <li className="menu-title">ХЭРЭГЛЭГЧДИЙН МЭДЭЭЛЭЛ</li>
                    {/* <ul className="nav-second-level" aria-expanded="true">
                        <li> */}
                                <Link to="/approved" className="side-nav-link-ref">
                                    <i class="fas fa-heartbeat"></i>&nbsp;
                                    <span className="badge badge-warning badge-pill float-right">{props.total}</span>
                                    <span style={sideStyle}> Хүсэлтүүд </span>
                                </Link>
                        {/* </li>
                        <li>         */}
                                <Link to="/insured" className="side-nav-link-ref">
                                    <i class="fas fa-map-marker-alt"></i>&nbsp;
                                    <span className="badge badge-success badge-pill float-right">{props.totalInsured}</span>
                                    <span style={sideStyle}> Гэрээ байгуулагдсан </span>
                                </Link>
                        {/* </li>
                        <li>          */}
                                <Link to="/cancelled" className="side-nav-link-ref">
                                    <i class="fas fa-ban"></i>&nbsp;
                                    <span className="badge badge-secondary badge-pill float-right">{props.totalCancel}</span>
                                    <span style={sideStyle}> Цуцлагдсан </span>
                                </Link>
                        {/* </li>
                        <li>          */}
                                <Link to="/cancel_request" className="side-nav-link-ref">
                                    <i class="fas fa-times-circle"></i>&nbsp;
                                    <span className="badge badge-secondary badge-pill float-right">{props.totalCancel}</span>
                                    <span style={sideStyle}> Цуцлах хүсэлт </span>
                                </Link>
                        {/* </li>
                        <li>   */}
                                <Link to="/exceeded" className="side-nav-link-ref">
                                    <i class="fas fa-hourglass-end"></i>&nbsp;
                                    <span className="badge badge-danger badge-pill float-right">{props.totalExceed}</span>
                                    <span style={sideStyle}> Хугацаа хэтэрсэн </span>
                                </Link>
                        {/* </li>
                        <li>   */}
                                <Link to="/ended" className="side-nav-link-ref">
                                    <i class="far fa-clock"></i>&nbsp;
                                    <span className="badge badge-danger badge-pill float-right">{props.totalEnd}</span>
                                    <span style={sideStyle}> Хугацаа дууссан </span>
                                </Link>
                        {/* </li> */}
                    {/* </ul> */}
                </li><hr/>
                <li className="menu-title">БУСАД</li>
                <li>
                    <Link to="/issues" className="waves-effect has-dropdown" aria-expanded="false">
                        <i class="far fa-comment-dots"></i>
                        <span style={sideStyle}> Санал гомдол </span>
                    </Link>
                </li>
                <li>
                    <Link to="/coupons" className="waves-effect has-dropdown" aria-expanded="false">
                        <i class="fas fa-gift"></i>
                        <span style={sideStyle}> Купон удирдлага </span>
                    </Link>
                </li>
            </ul>
        </div>
        <div className="clearfix"></div>
    </React.Fragment>
}

class Sidebar extends Component {

    constructor(props) {
        super(props);
        this.handleOtherClick = this.handleOtherClick.bind(this);
        this.initMenu = this.initMenu.bind(this);       
    }

    componentWillMount = () => {
        document.addEventListener('mousedown', this.handleOtherClick, false);
    }
    
    componentDidMount = () => {
        this.initMenu();
        this.props.getApprovalStarted();
        this.props.getInsuredStarted();
        this.props.getCancelStarted();
        this.props.getCancelRequestStarted();
        this.props.getExceedStarted();
        this.props.getEndStarted();
        this.props.getSetStarted();
        this.props.getIssueStarted();
        this.props.getCouponStarted();
    }

    componentDidUpdate = (prevProps) => {
        if (this.props.isCondensed !== prevProps.isCondensed) {
            if (prevProps.isCondensed) {
                document.body.classList.remove("sidebar-enable");
                document.body.classList.remove("enlarged");
            } else {
                document.body.classList.add("sidebar-enable");
                const isSmallScreen = window.innerWidth < 768;
                if (!isSmallScreen) {
                    document.body.classList.add("enlarged");
                }
            }            
            
            this.initMenu();
        }
    }

    componentWillUnmount = () => {
        document.removeEventListener('mousedown', this.handleOtherClick, false);
    }

    handleOtherClick = (e) => {
        if (this.menuNodeRef.contains(e.target))
            return;
        // else hide the menubar
        document.body.classList.remove('sidebar-enable');
    }

    initMenu = () => {
        // render menu
        new MetisMenu("#side-menu");
        var links = document.getElementsByClassName('side-nav-link-ref');
        var matchingMenuItem = null;
        for (var i = 0; i < links.length; i++) {
            if (this.props.location.pathname === links[i].pathname) {
                matchingMenuItem = links[i];
                break;
            }
        }

        if (matchingMenuItem) {
            matchingMenuItem.classList.add('active');
            var parent = matchingMenuItem.parentElement;

            if (parent) {
                parent.classList.add('active');
                const parent2 = parent.parentElement;
                if (parent2) {
                    parent2.classList.add('in');
                }
                const parent3 = parent2.parentElement;
                if (parent3) {
                    parent3.classList.add('active');
                    var childAnchor = parent3.querySelector('.has-dropdown');
                    if (childAnchor) childAnchor.classList.add('active');
                }

                const parent4 = parent3.parentElement;
                if (parent4)
                    parent4.classList.add('in');
                const parent5 = parent4.parentElement;
                if (parent5)
                    parent5.classList.add('active');
            }
        }
    }

    render() {
        const isCondensed = this.props.isCondensed || false;
        return (
            <React.Fragment>
                <div className='left-side-menu' ref={node => this.menuNodeRef = node}>
                    {!isCondensed && <PerfectScrollbar><SideNavContent 
                                                        total={this.props.approvalState.total} 
                                                        totalInsured={this.props.insuredState.total}  
                                                        totalCancel={this.props.cancelState.total}  
                                                        totalCancel={this.props.cancelRequestState.total}
                                                        totalExceed={this.props.exceedState.total}  
                                                        totalEnd={this.props.endState.total}  
                                                        /></PerfectScrollbar>}
                    {isCondensed && <SideNavContent 
                                    total={this.props.cancelState.total} 
                                    totalInsured={this.props.insuredState.total} 
                                    totalCancel={this.props.cancelState.total} 
                                    totalCancel={this.props.cancelRequestState.total} 
                                    totalExceed={this.props.exceedState.total} 
                                    totalEnd={this.props.endState.total}  
                                     />}
                </div>
            </React.Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getApprovalStarted,
    getInsuredStarted,
    getCancelStarted,
    getCancelRequestStarted,
    getExceedStarted,
    getEndStarted,
    getSetStarted,
    getIssueStarted,
    getCouponStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        approvalState: state.Approval,
        insuredState: state.Insured,
        cancelState: state.Cancel,
        cancelRequestState: state.CancelRequest,
        exceedState: state.Exceed,
        endState: state.End,
        setState: state.Set,
        issueState: state.Issue,
        couponState: state.Coupon,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
