import React, { Component } from 'react';
import axios from 'axios';

/**
 * Renders the Footer
 */
class Footer extends Component {

       constructor(props) {
        super(props);
        this.state = {
            approvals: [],
            cancel: [],
        };
    }

     componentDidMount() {
        axios.get('http://localhost:8000/api/approvals')
        .then(response => {
            this.setState({approvals: response.data})
        })  
        .catch(error => {
        console.log(error)
        this.setState({errorMsg: 'Error retreiving data '})
        })
    }
    
    render() {

    const sideStyle = {
        fontSize: "14px"
    }
        return (
            <footer className="footer">
                <div className="container-fluid">
                    <div className="row" style={sideStyle}>
                        <div className="col-md-4">
                            2019 - 2020 &copy; <a href="https://insur.mn/">ИНСУР ХХК</a>
                        </div>
                        <div className="col-md-4">
                            <p>Баталгаажсан: {this.state.approvals.length}&nbsp;&nbsp;&nbsp;&nbsp;
                            Цуцлагдсан: {this.state.cancel.length}&nbsp;&nbsp;&nbsp;&nbsp;   
                            Хугацаа хэтэрсэн: 45</p>
                        </div>
                        <div className="col-md-4">
                            <div className="text-md-right footer-links d-none d-sm-block">
                                <a href="https://insur.mn/">Холбоо барих</a>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer;