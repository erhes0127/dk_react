import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { UncontrolledDropdown, DropdownMenu, DropdownToggle, DropdownItem } from 'reactstrap';

import NotificationDropdown from './NotificationDropdown';
import ProfileDropdown from './ProfileDropdown';
import logoSm from '../assets/images/logo-sm.png';
import logo from '../assets/images/insurw.png';
import profilePic from '../assets/images/users/user-8.jpg';

const ProfileMenus = [{
  label: ' Гарах',
  icon: 'fe-log-out',
  redirectTo: "/logout",
  hasDivider: true
}]



class Topbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      query: '',
      results: []
    };
  }

  handleInputChange = () => {
   this.setState({
     query: this.search.value
   })
 }



  render() {
    return (
      <React.Fragment>
        <div className="navbar-custom">
          <ul className="list-unstyled topnav-menu float-right mb-0">

            <li className="d-none d-sm-block">
              <form className="app-search">
                <div className="app-search-box">
                  <div className="input-group">
                    <input type="text" className="form-control" placeholder="Хайлт ..." 
                    ref={input => this.search = input}
                    onChange={this.handleInputChange}/>
                    <p>{this.state.query}</p>
                    <div className="input-group-append">
                      <button className="btn" type="submit">
                        <i className="fe-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </li>

            <li>
              <ProfileDropdown profilePic={profilePic} menuItems={ProfileMenus} username={'User'} />
            </li>

            <li className="dropdown notification-list">
              <button className="btn btn-link nav-link right-bar-toggle waves-effect waves-light" onClick={this.props.rightSidebarToggle}>
                <i className="fe-settings noti-icon"></i>
              </button>
            </li>
          </ul>

          <div className="logo-box">
            <Link to="/chart" className="logo text-center">
              <span className="logo-lg">
                <img src={logo} alt="" height="50" />
              </span>
              <span className="logo-sm">
                <img src={logoSm} alt="" height="50" />
              </span>
            </Link>
          </div>

          <ul className="list-unstyled topnav-menu topnav-menu-left m-0">
            <li>
              <button className="button-menu-mobile waves-effect waves-light" onClick={this.props.menuToggle}>
                <i className="fe-menu"></i>
              </button>
            </li>
            
          </ul>
        </div>
      </React.Fragment >
    );
  }
}


export default connect()(Topbar);

