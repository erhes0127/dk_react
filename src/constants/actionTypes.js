/* AUTH */
export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILED = 'LOGIN_USER_FAILED';
export const REGISTER_USER = 'REGISTER_USER';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILED = 'REGISTER_USER_FAILED';
export const LOGOUT_USER = 'LOGOUT_USER';
export const FORGET_PASSWORD = 'FORGET_PASSWORD';
export const FORGET_PASSWORD_SUCCESS = 'FORGET_PASSWORD_SUCCESS';
export const FORGET_PASSWORD_FAILED = 'FORGET_PASSWORD_FAILED';

export const GET_APPROVAL_STARTED = 'GET_APPROVAL_STARTED';
export const GET_APPROVAL_SUCCESS = 'GET_APPROVAL_SUCCESS';
export const GET_APPROVAL_FAILED = 'GET_APPROVAL_FAILED';

export const DELETE_APPROVAL_STARTED = 'DELETE_APPROVAL_STARTED';
export const DELETE_APPROVAL_SUCCESS = 'DELETE_APPROVAL_SUCCESS';
export const DELETE_APPROVAL_FAILED = 'DELETE_APPROVAL_FAILED';

export const GET_INSURED_STARTED = 'GET_INSURED_STARTED';
export const GET_INSURED_SUCCESS = 'GET_INSURED_SUCCESS';
export const GET_INSURED_FAILED = 'GET_INSURED_FAILED';

export const GET_CANCEL_STARTED = 'GET_CANCEL_STARTED';
export const GET_CANCEL_SUCCESS = 'GET_CANCEL_SUCCESS';
export const GET_CANCEL_FAILED = 'GET_CANCEL_FAILED';

export const GET_CANCEL_REQUEST_STARTED = 'GET_CANCEL_REQUEST_STARTED';
export const GET_CANCEL_REQUEST_SUCCESS = 'GET_CANCEL_REQUEST_SUCCESS';
export const GET_CANCEL_REQUEST_FAILED = 'GET_CANCEL_REQUEST_FAILED';

export const GET_EXCEED_STARTED = 'GET_EXCEED_STARTED';
export const GET_EXCEED_SUCCESS = 'GET_EXCEED_SUCCESS';
export const GET_EXCEED_FAILED = 'GET_EXCEED_FAILED';

export const GET_END_STARTED = 'GET_END_STARTED';
export const GET_END_SUCCESS = 'GET_END_SUCCESS';
export const GET_END_FAILED = 'GET_END_FAILED';

export const GET_SET_STARTED = 'GET_SET_STARTED';
export const GET_SET_SUCCESS = 'GET_SET_SUCCESS';
export const GET_SET_FAILED = 'GET_SET_FAILED';

export const GET_ISSUE_STARTED = 'GET_ISSUE_STARTED';
export const GET_ISSUE_SUCCESS = 'GET_ISSUE_SUCCESS';
export const GET_ISSUE_FAILED = 'GET_ISSUE_FAILED';

export const GET_COUPON_STARTED = 'GET_COUPON_STARTED';
export const GET_COUPON_SUCCESS = 'GET_COUPON_SUCCESS';
export const GET_COUPON_FAILED = 'GET_COUPON_FAILED';