import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom'

import { Container, Row, Col, Card, CardBody, Label, FormGroup, Button, Alert } from 'reactstrap';
import { AvForm, AvField, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';

import { loginUser } from '../../redux/actions';
import { isUserAuthenticated } from '../../helpers/authUtils';
import Loader from '../../components/Loader';
import logo from '../../assets/images/insur.png';
import axios from 'axios';

class Login extends Component {
    _isMounted = false;

    constructor(props) {
        super(props);
        
        this.handleValidSubmit = this.handleValidSubmit.bind(this);
        this.state = {
            isLoggedIn: false,
            email: [],
            password: []
        }
    }

    componentDidMount() {
        this._isMounted = true;
        var formData = new FormData();
        // formData.append("email", email);
        // formData.append("password", password);

        axios.get('http://localhost:8000/api/user/login/', formData)
        .then(response => {
            console.log(response)
        this.setState({users: response.data})
        })
        .catch(error => {
            console.log(error)
        this.setState({errorMsg: 'Error retreiving data '})
        })
        // document.body.classList.add('authentication-bg');
        // document.body.classList.add('authentication-bg-pattern');
    }

    // componentWillUnmount() {
    //     this._isMounted = false;

    //     document.body.classList.remove('authentication-bg');
    //     document.body.classList.remove('authentication-bg-pattern');
    // }


    handleValidSubmit = (event, users) => {
        this.props.loginUser(users.email, users.password, this.props.history);
    }


    renderRedirectToRoot = () => {
        const isAuthTokenValid = isUserAuthenticated();
        if (isAuthTokenValid) {
            return <Redirect to='/chart' />
        }
    }

    render() {
        const isAuthTokenValid = isUserAuthenticated();
        return (
            <React.Fragment>

                {this.renderRedirectToRoot()}

                {(this._isMounted || !isAuthTokenValid) && <div className="account-pages mt-5 mb-5">
                    <Container>
                        <Row className="justify-content-center">
                            <Col md={8} lg={6} xl={5} >
                                <Card className="bg-pattern">
                                    <CardBody className="p-4 position-relative">
                                        { /* preloader */}
                                        {this.props.loading && <Loader />}

                                        <div className="text-center w-75 m-auto">
                                            <a href="/login">
                                                <span><img src={logo} alt="" height="50" /></span>
                                            </a>
                                            <p className="text-muted mb-4 mt-3">Даатгалын компанийн нэгдсэн систем</p>
                                        </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        {this.props.error && <Alert color="danger" isOpen={this.props.error ? true : false}>
                                            <div>{this.props.error}</div>
                                        </Alert>}

                                        <AvForm onValidSubmit={this.handleValidSubmit}>
                                            <AvField name="email" label="Нэвтрэх нэр" placeholder="Нэвтрэх нэр ... " value={this.state.email} required />

                                            <AvGroup className="mb-3">
                                                <Label for="password">Нууц үг</Label>
                                                <AvInput type="password" name="password" id="password" placeholder="Нууц үг" value={this.state.password} required />
                                                <AvFeedback>Хоосон талбарыг бөглөнө үү !</AvFeedback>
                                            </AvGroup>

                                            <FormGroup>
                                                <Button color="primary" className="btn-block">Нэвтрэх</Button>
                                            </FormGroup>

                                        </AvForm>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>

                        <Row className="mt-3">
                            <Col className="col-12 text-center">
                                <p><Link to="/forget-password" className="text-50 ml-1">Нууц үг мартсан ?</Link></p>
                                {/* <p className="text-white-50"><Link to="/register" className="text-white ml-1"><b>Бүртгүүлэх</b></Link></p> */}
                            </Col>
                        </Row>

                    </Container>
                </div>}

                <footer className="footer footer-alt">
                    2019 - 2020 &copy; <Link to="https://insur.mn/get-app" className="text-50">ИНСУР ХХК</Link>
                </footer>
            </React.Fragment>
        )
    }
}


const mapStateToProps = (state) => {
    const { user, loading, error } = state.Auth;
    return { user, loading, error };
};

export default connect(mapStateToProps, { loginUser })(Login);
