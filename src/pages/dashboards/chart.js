import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { getLoggedInUser } from '../../helpers/authUtils';
import Loader from '../../components/Loader';
import {Pie, Doughnut, Bar, Line} from 'react-chartjs-2';
import axios from 'axios';
import { bindActionCreators } from 'redux';

import { getApprovalStarted } from '../../redux/approval/actions';
import { getInsuredStarted } from '../../redux/insured/actions';
import { getCancelStarted } from '../../redux/cancel/actions';
import { getCancelRequestStarted } from '../../redux/cancel_request/actions';
import { getExceedStarted } from '../../redux/exceed/actions';
import { getEndStarted } from '../../redux/end/actions';
import { getSetStarted } from '../../redux/set/actions';
import { getCouponStarted } from '../../redux/coupon/actions';

const data = {
  labels: ['1-р сар', '2-р сар', '3-р сар', '4-р сар', '5-р сар', '6-р сар', '7-р сар'],
  datasets: [
    {
      label: 'Даатгалтай хэрэглэгчид',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: '#C3A9D8',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: '#7B25C1',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 5,
      pointRadius: 8,
      pointHitRadius: 10,
      data: [26, 59, 80, 81, 56, 55, 40]
    },{
      label: 'Цуцлагдсан даатгал',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'red',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 5,
      pointRadius: 8,
      pointHitRadius: 10,
      data: [45, 39, 50, 71, 86, 35, 60]
    }
  ]
};

class DefaultDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: getLoggedInUser()
        };
    }

     componentDidMount = () => {
        const { datasets } = this.refs.chart.chartInstance.data
        this.props.getApprovalStarted();
        this.props.getInsuredStarted();
        this.props.getCancelStarted();
        this.props.getCancelRequestStarted();
        this.props.getExceedStarted();
        this.props.getEndStarted();
        this.props.getSetStarted();
        this.props.getCouponStarted();
    }

    render() {
        const date = new Date().getDate();
        const month = new Date().getMonth() + 1;
        const year = new Date().getFullYear();
        // const hours = new Date().getHours();
        // const min = new Date().getMinutes();
        const approveCard = {
            background: '#2FB3E2',
            
        }
        const insuredCard = {
            background: '#1E9AC5',
        }
        const cancelCard = {
            background: '#ffc107',
        }
        const allCard = {
            background: '#f86c6b',
        }
        const couponCard = {
            background: '#f2e5c2',
        }
        return (
            <React.Fragment>
            <div>
              <h4 class="float-left">Нэгдсэн самбар</h4>
              <span className="float-right">
                {year} оны {month}-р сарын {date}
              </span>
             </div><br /><br />
                <div>
                <Row>
                    <Col>
                        <Card class="widget-rounded-circle card-box bg-blue">
                            <CardBody style={approveCard}>
                                <Row>
                                    <Col lg={6}>
                                    <div class="avatar-lg rounded-circle border-white border">
                                        <i class="fe-heart font-22 avatar-title text-white"></i>
                                    </div>
                                    </Col>
                                    <Col lg={6}>
                                    <div class="text-right">
                                        <h3 class="text-white mt-1">{this.props.approvalState.total}</h3>
                                        <p class="text-white mb-1 text-truncate">Хүсэлтүүд</p>
                                    </div>
                                    
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <CardBody style={insuredCard}>
                                <Row>
                                    <Col lg={6}>
                                    <div class="avatar-lg rounded-circle border-white border">
                                        <i class="fas fa-map-marker-alt font-22 avatar-title text-white"></i>
                                    </div>
                                    </Col>
                                    <Col lg={6}>
                                    <div class="text-right">
                                        <h3 class="text-white mt-1">{this.props.insuredState.total}</h3>
                                        <p class="text-white mb-1 text-truncate">Гэрээ байгуулагдсан</p>
                                    </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <CardBody style={cancelCard}>
                                <Row>
                                    <Col lg={6}>
                                    <div class="avatar-lg rounded-circle bg-soft-white border-white border">
                                        <i class="fas fa-ban font-22 avatar-title text-white"></i>
                                    </div>
                                    </Col>
                                    <Col lg={6}>
                                    <div class="text-right">
                                        <h3 class="text-white mt-1">{this.props.cancelState.total}</h3>
                                        <p class="text-white mb-1 text-truncate">Цуцлагдсан</p>
                                    </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                        <Card>
                            <CardBody style={allCard}>
                                <Row>
                                    <Col lg={6}>
                                    <div class="avatar-lg rounded-circle bg-soft-white border-white border">
                                        <i class="fe-shopping-cart font-22 avatar-title text-white"></i>
                                    </div>
                                    </Col>
                                    <Col lg={6}>
                                    <div class="text-right">
                                        <h3 class="text-white mt-1">{this.props.approvalState.total + this.props.insuredState.total + this.props.cancelState.total}</h3>
                                        <p class="text-white mb-1 text-truncate">Нийт</p>
                                    </div>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col lg={4}>
                        <Card>
                            <CardBody>
                            <div class="card-box">
                                <div class="widget-chart text-center">
                                    <h5 class="text-muted mt-3">Инсур мэдээллийн самбар</h5>
                                    <h2>some info</h2>
                                    <p class="text-muted w-75 mx-auto sp-line-2">
                                    Traditional heading elements are designed to work best in the meat of your page content.
                                    </p>
                                </div>
                            </div>
                            </CardBody>
                        </Card>
                        <Card>
                            <CardBody><br />
                            <div class="card-box">
                                <div class="avatar-lg rounded-circle bg-soft-success border-success border float-left">
                                    <i class="fas fa-gift font-22 avatar-title text-success"></i>
                                </div>
                                {/* <h2 class="header-title float-right">Купон ашиглалтын статистик </h2><br /> */}
                                <div class="widget-chart">
                                    <div class="row mt3">
                                        <div class="col-6 text-center">
                                            <h6 class="text-muted mb-1 text-truncate">Ашиглагдсан</h6>
                                            <h2>{this.props.couponState.total}</h2>
                                        </div>
                                        <div class="col-6 text-center">
                                            <h6 class="text-muted mb-1 text-truncate">Ашиглагдаагүй</h6>
                                            <h2>{this.props.couponState.data.is_active == 0 ? 
                                            this.props.couponState.data.is_active : '0'}</h2>
                                        </div>
                                    </div>
                                    <p class="text-muted w-75 mx-auto sp-line-2">
                                    
                                    </p>
                                </div>
                            </div>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col lg={8}>
                        <Card>
                            <CardBody>
                            <div class="card-box">
                                {/* <h3 class="header-title mb-3">Даатгал борлуулалтын анализ</h3> */}
                                <Line ref="chart" data={data} />
                            </div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                    
                </div>
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getApprovalStarted,
    getInsuredStarted,
    getCancelStarted,
    getCancelRequestStarted,
    getExceedStarted,
    getEndStarted,
    getSetStarted,
    getCouponStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        approvalState: state.Approval,
        insuredState: state.Insured,
        cancelState: state.Cancel,
        cancelRequestState: state.CancelRequest,
        exceedState: state.Exceed,
        endState: state.End,
        setState: state.Set,
        couponState: state.Coupon,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultDashboard);

