import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody } from 'reactstrap';

import { getLoggedInUser } from '../../helpers/authUtils';
import Loader from '../../components/Loader';

// import { makeStyles } from '@material-ui/core/styles';
// import Table from '@material-ui/core/Table';
import Table from 'react-bootstrap/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableHead from '@material-ui/core/TableHead';
// import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import { bindActionCreators } from 'redux';
import { getIssueStarted } from '../../redux/issue/actions';
import { Button, ButtonToolbar } from 'react-bootstrap';

class Issues extends Component {

  constructor(props) {
    super(props)
    this.state = {
      issues: [],
      errorMsg: '',
      title: '',
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3

    }
    this.handlePageChange = this.handlePageChange.bind(this)
  }

  handlePageChange(pageNumber) {
    // axios.get('http://localhost:8000/api/issues?page='+pageNumber)
    // .then(response => {
    //   this.setState({
    //     issues: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //     })
    // })
  }

  componentDidMount() {
    // axios.get('http://localhost:8000/api/issues')
    // .then(response => {
    //   console.log(response)
    //   this.setState({
    //     issues: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //   })
    // })
    // .catch(error => {
    //   console.log(error)
    //   this.setState({errorMsg: 'Error retreiving data '})
    // })
  }

  

    render() {
      const {errorMsg} = this.state;
      const issueState = this.props.issueState;
      const issues = issueState.data;
      const bodyStyle = {
        fontSize: "13px"
      }
      const divStyle = {
        fontSize: "14px"
      };
      const paperDesign = {
        borderBottomRightRadius: '20px',
        borderBottomLeftRadius: '20px'
      }
        return (
            <React.Fragment>
              <div>
              <h4 class="float-left">Хэрэглэгчдийн санал гомдол</h4>
              <span className="float-right">
                <Button variant="warning" size="sm">Төлөв</Button>
              </span>
             </div><br /><br />
              <Paper style={paperDesign}>
                <Table size="md" responsive hover>
                  <thead style={divStyle}>
                    <tr>
                      <th>#</th>
                      <th>Хэрэглэгч</th>
                      <th>Компани</th>
                      <th>Багц</th>
                      <th>Санал гомдол</th>
                    </tr>
                  </thead>
                  <tbody style={bodyStyle}>
                  {(issues.length > 0) ?
                    issues.map(issue => (
                      <tr key={issue.id}>  
                        <td>{issues.indexOf(issue)+1}</td>
                        <td>{issue.firstname}</td>
                        <td>{issue.company_name}</td>
                        <td>{issue.set_name}</td>
                        <td>{issue.description} </td>
                      </tr>
                    )) : ''
                  }
                  </tbody>
                </Table>
              </Paper> <br/>

              <div class="d-flex justify-content-center">
                <Pagination 
                  activePage = {issueState.current_page}
                  itemsCountPerPage={issueState.per_page}
                  totalItemsCount={issueState.total}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.handlePageChange}
                  itemClass='page-item'
                  linkClass='page-link'
                />
              </div> <br />
                
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getIssueStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        issueState: state.Issue
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Issues);