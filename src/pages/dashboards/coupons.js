import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody } from 'reactstrap';

import { getLoggedInUser } from '../../helpers/authUtils';
import Loader from '../../components/Loader';
import axios from 'axios';
// import Table from '@material-ui/core/Table';
import Table from 'react-bootstrap/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableHead from '@material-ui/core/TableHead';
// import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { rootCertificates } from 'tls';
import { pseudoRandomBytes } from 'crypto';
import Pagination from 'react-js-pagination';
import { bindActionCreators } from 'redux';
import { getCouponStarted } from '../../redux/coupon/actions';
import { Button, ButtonToolbar } from 'react-bootstrap';
import Select from 'react-select';

class Coupon extends Component {

  constructor(props) {
    super(props)
    this.state = {
      coupons: [],
      errorMsg: '',
      title: '',
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3

    }
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  handlePageChange(pageNumber) {
    // axios.get('http://localhost:8000/api/coupons?page='+pageNumber)
    // .then(response => {
    //   this.setState({
    //     coupons: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //     })
    // })
  }

  componentDidMount() {
    // axios.get('http://localhost:8000/api/coupons')
    // .then(response => {
    //   console.log(response)
    //   this.setState({
    //     coupons: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //   })
    // })
    // .catch(error => {
    //   console.log(error)
    //   this.setState({errorMsg: 'Error retreiving data '})
    // })
  }


  

    render() {
      const {errorMsg} = this.state;
      const couponState = this.props.couponState;
      const coupons = couponState.data;

       const couponType = [
          { label: "Ашиглагдсан", value: 1},
          { label: "Ашиглагдаагүй", value: 0},
        ];
      const bodyStyle = {
        fontSize: "13px"
      }
      const divStyle = {
        fontSize: "14px"
      };
        return (
            <React.Fragment>
            <div>
            <h5 class="float-left"><i class="fas fa-gift font-22 text-success"></i>&nbsp;Купон удирдлага</h5>
              <span className="float-right">
              <Button variant="warning" size="sm">Шүүлтүүр</Button>
              {/* <Select options={couponType} />&nbsp;&nbsp; */}
              </span>
             </div><br /><br />
              <Paper>
                <Table size="md" responsive hover>
                  <thead style={divStyle}>
                    <tr>
                      <th>#</th>
                      <th>Багц</th>
                      <th>Купон код</th>
                      <th>Ашиглалт</th>
                      <th>Үүсгэсэн огноо</th>
                      <th>Ашиглагдсан огноо</th>
                      <th>Тооцоо</th>
                    </tr>
                  </thead>
                  <tbody style={bodyStyle}>
                  {(coupons.length > 0) ?
                    coupons.map(coupon => (
                        <tr key={coupon.id}>  
                          <td>{coupons.indexOf(coupon)+1}</td>
                          <td>{coupon.set_name}</td>
                          <td>{coupon.coupon_code}</td>
                          <td>{coupon.is_active == 1 ? 'Ашиглагдаагүй' : 'Ашиглагдсан'}</td>
                          <td>{coupon.created_at}</td>
                          <td>{coupon.used_at}</td>
                          <td>{coupon.company_name}</td>
                        </tr>
                    )) : ''
                  }
                  </tbody>
                </Table>
              </Paper> <br/>

              <div class="d-flex justify-content-center">
                <Pagination 
                  activePage = {couponState.current_page}
                  itemsCountPerPage = {couponState.per_page}
                  totalItemsCount = {couponState.total}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.handlePageChange}
                  itemClass='page-item'
                  linkClass='page-link'
                />
              </div> <br />
                
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getCouponStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        couponState: state.Coupon
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Coupon);