import React, { Component, useState, useEffect } from 'react';
import ReactDOM from "react-dom";
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { getLoggedInUser } from '../../helpers/authUtils';
import Loader from '../../components/Loader';
import { makeStyles } from '@material-ui/core/styles';
import Table from 'react-bootstrap/Table'
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import Pagination from 'react-js-pagination';
import PageItem from 'react-bootstrap/PageItem';  
import { Button, ButtonToolbar } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalFooter from 'react-bootstrap/ModalFooter';
import ModalDialog from 'react-bootstrap/ModalDialog';
import Form from 'react-bootstrap/Form';
import DatePicker from 'react-date-picker';
import "react-datepicker/dist/react-datepicker.css";
import { bindActionCreators } from 'redux';
import { getSetStarted } from '../../redux/set/actions';
import NumberFormat from 'react-number-format';
import { SketchPicker } from 'react-color';

function MyVerticallyCenteredModal(props) {

    return (<div>
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            <h5>Нөхөн олговорын хязгаарлалт оруулах</h5>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Даатгалын багц</Form.Label>
              <Form.Control type="text" placeholder={props.data} disabled/>
            </Form.Group>

            <Form.Group>
              <Form.Label>Мэс засал</Form.Label>
              <Form.Control type="text" placeholder={props.data} value="" />
            </Form.Group>
            <Form.Group>
              <Form.Label>Гэнэтийн осол</Form.Label>
              <Form.Control type="number" placeholder="Гэнэтийн осол" />
            </Form.Group>
            <Form.Group>
              <Form.Label>Амбулатори</Form.Label>
              <Form.Control type="number" placeholder="Амбулатори" />
            </Form.Group>
            <Form.Group>
              <Form.Label>Хэвтэн эмчлүүлэх</Form.Label>
              <Form.Control type="text" placeholder="Хэвтэн эмчлүүлэх" disabled/>
            </Form.Group>

          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={props.onHide}>Хаах</Button>
          <Button>Хадгалах</Button>
        </Modal.Footer>
      </Modal>
    </div>);
}


class Sets extends Component {

  constructor(props) {
    super(props)
    this.state = {
      sets: [],
      errorMsg: '',
      title: '',
      showSetup: false,
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3,
      setModalShow: false,
      modalShow: false,
      disabled: false,
      rows: [],
    }
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleHide = this.handleHide.bind(this);
  }

  toggleSetup(){
    this.setState({
      showSetup: !this.state.showSetup
    });
  }

  handleShow = (e, data) => {
   console.log('set modal show');
   this.setState({
     modalShow: true
     })
      const id = e.target.id;
     console.log(data);
  }
  
  handleHide = () => {
    console.log('closed');
    this.setState({modalShow: false})
    }

  onChange = date => this.setState({ date })

  handlePageChange(pageNumber) {
    // axios.get('http://localhost:8000/api/sets?page=' + pageNumber)
    // .then(response => {
    //   this.setState({
    //     sets: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //     })
    // })
  }

  // componentDidUpdate(prevProps, prevState, sets) {

  // }

  // componentWillUnmount() {
  //   this._isMounted = false;
  // }
  
    render() {
      const {errorMsg, modalShow} = this.state;
      const setState = this.props.setState;
      const sets = setState.data;

      const divStyle = {
          // background: '#307aff47',
          fontSize: "14px"
        };
      const bodyStyle = {
        fontSize: "13px"
      }
      const paperColor = {
        background: 'linear-gradient(rgba(201,211,228,45),transparent)',
        borderBottomRightRadius: '20px',
        borderBottomLeftRadius: '20px'
      }
        return (
            <React.Fragment>
              <h5>Багцийн мэдээлэл</h5><br />
              <Paper style={paperColor}>
                 <Table size="md" responsive hover>
                 {this.props.loading && <Loader />}
                  <thead style={divStyle}>
                    <tr size="sm">
                      <th>#</th>
                      <th>Багцын төрөл</th>
                      <th>Багцын нэр</th>
                      <th>Багцын төлбөр</th>
                      <th>Нөхөн олговор</th>
                      <th>Гэрээ</th>
                      <th className="d-flex justify-content-center">Багаж</th>
                    </tr>
                  </thead>
                  <tbody style={bodyStyle}>
                    {(sets.length > 0) ? 
                      sets.map(set => (
                        <tr key={set.id}>
                          <td>{sets.indexOf(set) + 1}</td>
                          <td>{set.type_name}</td>
                          <td>{set.set_name}</td>
                          <td><NumberFormat value={set.set_tax} displayType={'text'} thousandSeparator={true} prefix={'₮ '} /></td>
                          <td><NumberFormat value={set.claim} displayType={'text'} thousandSeparator={true} prefix={'₮ '} /></td>
                          {/* PIVOT?  */}
                          <td><a href={set.contract_term}>Хэрэглэгчтэй байгуулах гэрээ</a></td> 
                          <td className="d-flex justify-content-center"><ButtonToolbar>
                              <AddCircleOutlineOutlinedIcon variant="success"
                               onClick={this.handleShow}
                               id={set.id}
                               size="sm"
                               />
                              <MyVerticallyCenteredModal show={modalShow} onHide={this.handleHide} data={'dasfadf'} />
                              </ButtonToolbar>
                          </td>
                        </tr>
                      )) : ''
                    }
                  </tbody>
                </Table>
              </Paper><br/>
                <div className="d-flex justify-content-center">
                  <Pagination 
                    activePage = {this.state.activePage}
                    itemsCountPerPage={this.state.itemsCountPerPage}
                    totalItemsCount={this.state.totalItemsCount}
                    pageRangeDisplayed={this.state.pageRangeDisplayed}
                    onChange={this.handlePageChange}
                    itemClass='page-item'
                    linkClass='page-link'
                  />
                </div>
              </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getSetStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        setState: state.Set,
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Sets);
