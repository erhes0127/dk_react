import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { getLoggedInUser } from '../../helpers/authUtils';
// import Loader from '../../components/Loader';
// import { makeStyles } from '@material-ui/core/styles';
import Table from 'react-bootstrap/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableHead from '@material-ui/core/TableHead';
// import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import Form from 'react-bootstrap/Form';
import Pagination from 'react-js-pagination';
import { bindActionCreators } from 'redux';
import { getCancelRequestStarted } from '../../redux/cancel_request/actions';
import { Button, ButtonToolbar } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalFooter from 'react-bootstrap/ModalFooter'
import VisibilityIcon from '@material-ui/icons/Visibility';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import { MDBIcon, MDBBtn } from "mdbreact";

function FrontIdModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Иргэний үнэмлэхний урд талын зураг
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p>front id photo</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}

function RearIdModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Иргэний үнэмлэхний хойд талын зураг
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p>rear id photo</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.handleHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}

function CancelRequestModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Хувийн мэдээлэл 
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
              <Row xs="2">
                <Col>
                  <Form.Group>
                    <Form.Label>Ургийн овог</Form.Label>
                    <Form.Control type="text" placeholder={props.data} value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Нэр</Form.Label>
                    <Form.Control type="text" placeholder="Нэр" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Дугаар</Form.Label>
                    <Form.Control type="number" placeholder="Дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Цахим шуудан</Form.Label>
                    <Form.Control type="text" placeholder="Цахим шуудан" value="" disabled/>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group>
                    <Form.Label>Овог</Form.Label>
                    <Form.Control type="text" placeholder="Овог" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Регистрийн дугаар</Form.Label>
                    <Form.Control type="text" placeholder="Регистрийн дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Холбоо барих дугаар</Form.Label>
                    <Form.Control type="number" placeholder="Холбоо барих дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Гэрийн хаяг</Form.Label>
                    <Form.Control type="text" placeholder="Гэрийн хаяг" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    {/* <Button variant="secondary" onClick={props.handleCancelRequestHide}>Хаах</Button>&nbsp;&nbsp; */}
                  </Form.Group>
                </Col>
              </Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
}

function CancelRequestDelete(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Хүсэлт цуцлах
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form.Group>
              <Form.Label>Регистрийн дугаар</Form.Label>
              <Form.Control type="text" placeholder={props.data} value="" disabled/>
            </Form.Group>
            <Form.Group>
              <Form.Label>Шалтгаан</Form.Label>
              <Form.Control type="textarea" placeholder="Шалтгаан"/>
            </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
          <Button variant="danger">УСТГАХ</Button>
        </Modal.Footer>
      </Modal>
    );
}

class CancelRequest extends Component {

  constructor(props) {
    super(props)
    this.state = {
      cancel_request: [],
      errorMsg: '',
      title: '',
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3,
      frontShow: false,
      rearShow: false,
      cancelRequestShow: false,
      deleteShow: false,

    }
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleRearShow = this.handleRearShow.bind(this);
    this.handleRearHide = this.handleRearHide.bind(this);
    this.handleFrontShow = this.handleFrontShow.bind(this);
    this.handleFrontHide = this.handleFrontHide.bind(this);
    this.handleCancelRequestShow = this.handleCancelRequestShow.bind(this);
    this.handleCancelRequestHide = this.handleCancelRequestHide.bind(this);
    this.handleDeleteShow = this.handleDeleteShow.bind(this);
    this.handleDeleteHide = this.handleDeleteHide.bind(this);

  }

  handleDeleteShow = (e) => {
    this.setState({
      deleteShow: true
    });
    const id = e.target.id;
    console.log(`fuck this ${id}`);
  }

  handleDeleteHide = ( e) => {this.setState({deleteShow: false});}

  handleCancelRequestShow = (e) => {
    this.setState({
      cancelRequestShow: true,
    });
    const id = e.target.id;
    console.log(id);
  }

  handleCancelRequestHide = () => {this.setState({cancelRequestShow: false});}

  handleRearShow = (e) => {
    console.log('rear id show')
    this.setState({
      rearShow: true
    });
    const id = e.target.id;
  }

  handleRearHide = () => {this.setState({rearShow: false});}

  handleFrontShow = (e) => {
    console.log('front id show')
    this.setState({
      frontShow: true
    });
    const id = e.target.id;
  }

  handleFrontHide = () => {this.setState({frontShow: false});}

  handlePageChange(pageNumber) {
    axios.get('http://localhost:8000/api/cancel_request?page='+pageNumber)
    .then(response => {
      this.setState({
        cancel_request: response.data.data,
        itemsCountPerPage: response.data.per_page,
        totalItemsCount: response.data.total,
        activePage: response.data.current_page
        })
    })
  }

  componentDidMount() {
    // axios.get('http://localhost:8000/api/cancel_request')
    // .then(response => {
    //   console.log(response)
    //   this.setState({
    //     cancel_request: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //   })
    // })
    // .catch(error => {
    //   console.log(error)
    //   this.setState({errorMsg: 'Error retreiving data '})
    // })
  }

  

    render() {
      const {errorMsg, rearShow, frontShow, cancelRequestShow, deleteShow} = this.state;
      const cancelRequestState = this.props.cancelRequestState;
      const cancel_request = cancelRequestState.data;
      const divStyle = {
        fontSize: "14px"
      };
      const bodyStyle = {
        fontSize: "13px"
      }
      const paperDesign = {
        borderBottomRightRadius: '20px',
        borderBottomLeftRadius: '20px'
      }
        return (
            <React.Fragment>
              <h4>Цуцлах хүсэлт</h4><br />
              <Paper style={paperDesign}>
                <Table size="md" responsive hover>
                  <thead style={divStyle}>
                    <tr>
                      <th>#</th>
                      <th>Нэр</th>
                      <th>РД</th>
                      <th>Нас</th>
                      <th>Төрөл</th>
                      <th>Утас</th>
                      <th>Ир/үнэмлэх</th>
                      <th>Сонг/Багц</th>
                      <th>Цуцлах шалтгаан</th>
                      <th>Хүс/Гар/Огноо</th>
                      <th>Цуцлах Хүс/Гар/Огноо</th>
                      <th>-</th>
                    </tr>
                  </thead>
                  <tbody style={bodyStyle}>
                  {(cancel_request.length > 0) ?
                    cancel_request.map(cancel_requests => (
                        <tr key={cancel_requests.id}>  
                          <td>{cancel_request.indexOf(cancel_requests)+1}</td>
                          <td>{cancel_requests.children_id = null ? cancel_requests.child_firstname : cancel_requests.firstname}</td>
                          <td>{cancel_requests.children_id = null ? cancel_requests.child_regnum : cancel_requests.regnum}</td>
                          <td>{cancel_requests.age}</td>
                          <td>{cancel_requests.gender == 0 ? <MDBIcon icon="female" size="lg"/> : <MDBIcon icon="male" size="lg"/>}</td>
                          <td>{cancel_requests.phone_number}</td>
                          <td>
                            <ButtonToolbar>
                              <a href="#" onClick={this.handleFrontShow} id={cancel_requests.id}>Урд</a> &nbsp;
                              <a href="#" onClick={this.handleRearShow} id={cancel_requests.id}>Ард</a>
                              <RearIdModal show={rearShow} onHide={this.handleRearHide} />
                              <FrontIdModal show={frontShow} onHide={this.handleFrontHide} />
                            </ButtonToolbar>
                          </td>
                          <td>{cancel_requests.set_name}</td>
                          {/* InsuranceStatus */}
                          <td>notes</td>
                          <td>{cancel_requests.created_at}</td>
                          <td>{cancel_requests.created_at}</td>
                          <td>
                            <ButtonToolbar>
                              <VisibilityIcon
                               onClick={this.handleCancelRequestShow}
                               id={cancel_requests.id}
                               size="sm"
                               />&nbsp;
                              <CancelRequestModal id={cancel_requests.id} show={cancelRequestShow} onHide={this.handleCancelRequestHide}/>

                              <HighlightOffIcon
                               onClick={this.handleDeleteShow}
                               id={cancel_requests.id}
                               size="sm"
                               color="warning"
                               />&nbsp;
                              <CancelRequestDelete show={deleteShow} onHide={this.handleDeleteHide} id={cancel_requests.id} data={cancel_requests.regnum}/>
                          </ButtonToolbar></td>
                        </tr>
                    )) : ''
                   }
                  </tbody>
                </Table>
              </Paper> <br/>

              <div class="d-flex justify-content-center">
                <Pagination 
                  activePage = {this.state.activePage}
                  itemsCountPerPage={this.state.itemsCountPerPage}
                  totalItemsCount={this.state.totalItemsCount}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.handlePageChange}
                  itemClass='page-item'
                  linkClass='page-link'
                />
              </div> <br />
                
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getCancelRequestStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        cancelRequestState: state.CancelRequest
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CancelRequest);

