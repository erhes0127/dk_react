import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { getLoggedInUser } from '../../helpers/authUtils';
import Loader from '../../components/Loader';
// import { makeStyles } from '@material-ui/core/styles';
import Table from 'react-bootstrap/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableHead from '@material-ui/core/TableHead';
// import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import { bindActionCreators } from 'redux';
import { getEndStarted } from '../../redux/end/actions';
import DatePicker from 'react-date-picker';
import { Button, ButtonToolbar } from 'react-bootstrap';
import NumberFormat from 'react-number-format';

class Ended extends Component {

  constructor(props) {
    super(props)
    this.state = {
      end: [],
      errorMsg: '',
      title: '',
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3

    }
    this.handlePageChange = this.handlePageChange.bind(this);

  }

  handlePageChange(pageNumber) {
    axios.get('http://localhost:8000/api/end?page='+pageNumber)
    .then(response => {
      this.setState({
        end: response.data.data,
        itemsCountPerPage: response.data.per_page,
        totalItemsCount: response.data.total,
        activePage: response.data.current_page
        })
    })
  }

  onBeginChange = beginDate => this.setState({ beginDate })
  onEndChange = endDate => this.setState({ endDate })

  componentDidMount() {
    // axios.get('http://localhost:8000/api/end')
    // .then(response => {
    //   console.log(response)
    //   this.setState({
    //     end: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //     })
    // })
    // .catch(error => {
    //   console.log(error)
    //   this.setState({errorMsg: 'Error retreiving data '})
    // })
  }

  

    render() {
      const {errorMsg} = this.state;
      const endState = this.props.endState;
      const end = endState.data;
      const divStyle = {
        fontSize: "14px"
      };
      const bodyStyle = {
        fontSize: "13px"
      }
        return (
            <React.Fragment>
              <div>
              <h4 class="float-left">Төлбөр төлөх хугацаа дууссан даатгалтай иргэд</h4>
              <span className="float-right">
                  <DatePicker
                    onChange={this.onBeginChange}
                    value={this.state.beginDate}
                  />&nbsp;&nbsp;
                  <DatePicker
                    onChange={this.onEndChange}
                    value={this.state.endDate}
                  />&nbsp;&nbsp;
                  <Button variant="warning" size="sm">Экспортлох</Button>
                </span>
              </div><br /><br />
              <Paper>
                <Table size="md" responsive hover>
                  <thead style={divStyle}>
                    <tr>
                      <th>#</th>
                      <th>Ургийн овог</th>
                      <th>Овог</th>
                      <th>Нэр</th>
                      <th>РД</th>
                      <th>Утас</th>
                      <th>Гэр/Дугаар</th>
                      <th>Сонг/Багц</th>
                      <th>Нөхөн олговор</th>
                      <th>ГБО</th>
                      <th>ГДО</th>
                    </tr>
                  </thead>
                  <tbody style={bodyStyle}>
                  {(end.length > 0) ?
                    end.map(ended => (
                        <tr key={ended.id}>  
                          <td>{end.indexOf(ended)+1}</td>
                          <td>{ended.children_id = null ? ended.children_familyname : ended.familyname}</td>
                          <td>{ended.children_id = null ? ended.children_lastname : ended.lastname}</td>
                          <td>{ended.children_id = null ? ended.children_firstname : ended.firstname}</td>
                          <td>{ended.children_id = null ? ended.children_regnum : ended.regnum}</td>
                          <td>{ended.phone_number}</td>
                          <td>{ended.contract_number}</td>
                          <td>{ended.set_name}</td>
                          <td><NumberFormat value={ended.cover_balance} displayType={'text'} thousandSeparator={true} prefix={'₮'} /></td>
                          <td>{ended.contract_created_at}</td>
                          <td>{ended.contract_expires_at}</td>
                        </tr>
                    )) : ''
                   }
                  </tbody>
                </Table>
              </Paper><br/>

              <div class="d-flex justify-content-center">
                <Pagination 
                  activePage = {this.state.activePage}
                  itemsCountPerPage={this.state.itemsCountPerPage}
                  totalItemsCount={this.state.totalItemsCount}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.handlePageChange}
                  itemClass='page-item'
                  linkClass='page-link'
                />
              </div> <br />
                
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getEndStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        endState: state.End
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Ended);
