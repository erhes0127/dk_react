import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { Row, Col, Card, CardBody } from 'reactstrap';

import { getLoggedInUser } from '../../helpers/authUtils';
// import Loader from '../../components/Loader';

// import { makeStyles } from '@material-ui/core/styles';
// import Table from '@material-ui/core/Table';
import Table from 'react-bootstrap/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableHead from '@material-ui/core/TableHead';
// import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import { Button, ButtonToolbar } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalFooter from 'react-bootstrap/ModalFooter'
import DatePicker from 'react-date-picker';
import "react-datepicker/dist/react-datepicker.css";
import { bindActionCreators } from 'redux';
import { getCancelStarted } from '../../redux/cancel/actions';
import { MDBIcon, MDBBtn } from "mdbreact";

function FrontIdModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Иргэний үнэмлэхний урд талын зураг
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p>front id photo</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}

function RearIdModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Иргэний үнэмлэхний хойд талын зураг
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p>rear id photo</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.handleHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}

class Cancelled extends Component {

  constructor(props) {
    super(props)
    this.state = {
      cancel: [],
      errorMsg: '',
      title: '',
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3,
      frontShow: false,
      rearShow: false,
      beginDate: new Date(),
      endDate: new Date(),

    }
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleRearShow = this.handleRearShow.bind(this);
    this.handleRearHide = this.handleRearHide.bind(this);
    this.handleFrontShow = this.handleFrontShow.bind(this);
    this.handleFrontHide = this.handleFrontHide.bind(this);
  }

  handleRearShow = () => {
    console.log('rear id show')
    this.setState({
      rearShow: true
    });
  }

  handleRearHide = () => {
    this.setState({
      rearShow: false
    });
  }

  handleFrontShow = () => {
    console.log('front id show')
    this.setState({
      frontShow: true
    });
  }

  handleFrontHide = () => {
    this.setState({
      frontShow: false
    });
  }

  onBeginChange = beginDate => this.setState({ beginDate })
  onEndChange = endDate => this.setState({ endDate })
  


    handlePageChange(pageNumber) {
      axios.get('http://localhost:8000/api/cancel?page='+pageNumber)
      .then(response => {
        this.setState({
          cancel: response.data.data,
          itemsCountPerPage: response.data.per_page,
          totalItemsCount: response.data.total,
          activePage: response.data.current_page
          })
      })
    }

  componentDidMount() {
    // axios.get('http://localhost:8000/api/cancel')
    // .then(response => {
    //   console.log(response)
    //   this.setState({
    //     cancel: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //   })
    // })
    // .catch(error => {
    //   console.log(error)
    //   this.setState({errorMsg: 'Error retreiving data '})
    // })
  }

  

    render() {
      const {errorMsg, rearShow, frontShow} = this.state;
      const cancelState = this.props.cancelState;
      const cancel = cancelState.data;
      const divStyle = {
        fontSize: "14px"
      };
      const bodyStyle = {
        fontSize: "13px"
      }
      const paperDesign = {
        borderBottomRightRadius: '20px',
        borderBottomLeftRadius: '20px'
      }
        return (
            <React.Fragment>
            <div>
              <h4 class="float-left">Цуцлагдсан хүсэлт</h4>
              <span className="float-right">
                <DatePicker
                    onChange={this.onBeginChange}
                    value={this.state.beginDate}
                />&nbsp;&nbsp;
                <DatePicker
                  onChange={this.onEndChange}
                  value={this.state.endDate}
                />&nbsp;&nbsp;
                <Button variant="warning" size="sm">Экспортлох</Button>
              </span>
             </div><br /><br />
              <Paper style={paperDesign}>
                <Table size="md" responsive hover>
                  <thead style={divStyle}>
                    <tr>
                      <th>#</th>
                      <th>Ургийн овог</th>
                      <th>Овог</th>
                      <th>Нэр</th>
                      <th>РД</th>
                      <th>Нас</th>
                      <th>Төрөл</th>
                      <th>Утас</th>
                      <th>Ир/үнэмлэх</th>
                      <th>Хаяг</th>
                      <th>Сонг/Багц</th>
                      <th>Хүс/Гар/Огноо</th>
                    </tr>
                  </thead>
                  <tbody style={bodyStyle}>
                    { (cancel.length > 0) ?
                      cancel.map(cancelled => (
                          <tr key={cancelled.id}>  
                            <td>{cancel.indexOf(cancelled)+1}</td>
                            <td>{cancelled.children_id = null ? cancelled.child_familyname : cancelled.familyname}</td>
                            <td>{cancelled.children_id = null ? cancelled.child_lastname : cancelled.lastname}</td>
                            <td>{cancelled.children_id = null ? cancelled.child_firstname : cancelled.firstname}</td>
                            <td>{cancelled.children_id = null ? cancelled.child_regnum : cancelled.regnum}</td>
                            <td>{cancelled.age}</td>
                            <td>{cancelled.gender == 0 ? <MDBIcon icon="female" size="lg"/> : <MDBIcon icon="male" size="lg"/>}</td>
                            <td>{cancelled.phone_number}</td>
                            <td>
                              <ButtonToolbar>
                              
                                <a href="#" onClick={this.handleFrontShow} id={cancelled.id}>Урд</a> &nbsp; 
                                <a href="#" onClick={this.handleRearShow} id={cancelled.id}>Ард</a> 
                                
                              <RearIdModal show={rearShow} onHide={this.handleRearHide} />
                              <FrontIdModal show={frontShow} onHide={this.handleFrontHide} />
                              </ButtonToolbar>
                            </td>
                            <td>{cancelled.address}</td>
                            <td>{cancelled.set_name}</td>
                            <td>{cancelled.created_at}</td>
                          </tr>
                      )) : ''
                    }
                  </tbody>
                </Table>
              </Paper> <br/>

              <div class="d-flex justify-content-center">
                <Pagination 
                  activePage = {cancelState.current_page}
                  itemsCountPerPage={cancelState.per_page}
                  totalItemsCount={cancelState.total}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.handlePageChange}
                  itemClass='page-item'
                  linkClass='page-link'
                />
              </div> <br />
                
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getCancelStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
       cancelState: state.Cancel
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cancelled);
