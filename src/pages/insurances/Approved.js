import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { getLoggedInUser } from '../../helpers/authUtils';
import Loader from '../../components/Loader';
import axios from 'axios';
// import Table from '@material-ui/core/Table';
import Table from 'react-bootstrap/Table';
import Paper from '@material-ui/core/Paper';
// import { rootCertificates } from 'tls';
import { pseudoRandomBytes } from 'crypto';
import Pagination from 'react-js-pagination'; 
import { Button, ButtonToolbar } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalFooter from 'react-bootstrap/ModalFooter';
import DatePicker from 'react-date-picker';
import { bindActionCreators } from 'redux';
import { getApprovalStarted } from '../../redux/approval/actions';
import { deleteApprovalStarted } from '../../redux/approval/actions';
import Form from 'react-bootstrap/Form';
import EmailOutlinedIcon from '@material-ui/icons/EmailOutlined';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import NoteAddOutlinedIcon from '@material-ui/icons/NoteAddOutlined';
import { MDBIcon, MDBBtn } from "mdbreact";
import { Alert } from 'reactstrap';
import logo from '../../assets/images/insur.png';


const loading = () => <div></div>

function Contract(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Гэрээг баталгаажуулах
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form>
              <Row xs="3">
                <Col>
                  <Form.Group>
                    <Form.Label>Ургийн овог</Form.Label>
                    <Form.Control type="text" placeholder={props.data} value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Нэр</Form.Label>
                    <Form.Control type="text" placeholder="Нэр" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Нас</Form.Label>
                    <Form.Control type="number" placeholder="Нас" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Дугаар</Form.Label>
                    <Form.Control type="number" placeholder="Дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Цахим шуудан</Form.Label>
                    <Form.Control type="text" placeholder="Цахим шуудан" value="" disabled/>
                  </Form.Group>
                   <Form.Group>
                    <Form.Label>Гэрийн хаяг</Form.Label>
                    <Form.Control type="text" placeholder="Гэрийн хаяг" value="" disabled/>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group>
                    <Form.Label>Овог</Form.Label>
                    <Form.Control type="text" placeholder="Овог" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Регистрийн дугаар</Form.Label>
                    <Form.Control type="text" placeholder="Регистрийн дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Төрөл</Form.Label>
                    <Form.Control type="text" placeholder="Төрөл" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Холбоо барих дугаар</Form.Label>
                    <Form.Control type="number" placeholder="Холбоо барих дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Хэвтэн эмчлүүлэх</Form.Label>
                    <Form.Control type="text" placeholder="Хэвтэн эмчлүүлэх" value="" disabled/>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group>
                    <Form.Label>Гэрээний дугаар</Form.Label>
                    <Form.Control type="text" placeholder="Гэрээний дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Гэрээ байгуулагдсан огноо</Form.Label>
                    <Form.Control type="date"/>
                  </Form.Group>
                   <Button variant="secondary" onClick={props.onHide}>Хаах</Button>&nbsp;&nbsp;
                   <Button variant="warning">Гэрээ байгуулах</Button>
                </Col>
              </Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
}

function Notes(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Тэмдэглэл
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p className="float-left">Хэрэглэгч: firstname(phone_number)</p>
            <AddCircleOutlineOutlinedIcon variant="warning" className="float-right"/>
            <br /> <br />
            <p>Тэмдэглэлийн жагсаалт</p>
            <ul>
              <li>description notes</li>
            </ul>
        </Modal.Body>
      </Modal>
    );
}

function ContractOnWay(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Хүсэлт
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <img src={logo} alt="" height="50" />
            <p>Гэрээг хүргэгдэж буй төлөвт оруулах уу?</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
          <Button>Тийм</Button>
        </Modal.Footer>
      </Modal>
    );
}

function FrontIdModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Иргэний үнэмлэхний урд талын зураг
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p>front id photo</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}

function RearIdModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Иргэний үнэмлэхний хойд талын зураг
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <img src={logo} alt="" height="50" />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}

function CancelRequest(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Хүсэлт цуцлах
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form.Group>
              <Form.Label>Регистрийн дугаар</Form.Label>
              <Form.Control type="text" placeholder="Регистрийн дугаар" value="" disabled/>
            </Form.Group>
            <Form.Group>
              <Form.Label>Шалтгаан</Form.Label>
              <Form.Control type="textarea" placeholder="Шалтгаан"/>
            </Form.Group>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
          <Button variant="danger" onClick={props.onDelete}>ЦУЦЛАХ</Button>
        </Modal.Footer>
      </Modal>
    );
}

class Approved extends Component {

  constructor(props) {
    super(props)
    this.state = {
      approvals: [],
      errorMsg: '',
      showPopup: false,
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3,
      frontShow: false,
      rearShow: false,
      modalShow: false,
      contractShow: false,
      cRequestShow: false,
      noteShow: false,
      beginDate: new Date(),
      endDate: new Date(),
    }
    this.handlePageChange = this.handlePageChange.bind(this);  
    this.handleShow = this.handleShow.bind(this); 
    this.handleRearShow = this.handleRearShow.bind(this);
    this.handleRearHide = this.handleRearHide.bind(this);
    this.handleFrontShow = this.handleFrontShow.bind(this);
    this.handleFrontHide = this.handleFrontHide.bind(this);
    this.handleContractShow = this.handleContractShow.bind(this);
    this.handleContractHide = this.handleContractHide.bind(this);
    this.handleNoteShow = this.handleNoteShow.bind(this);
    this.handleNoteHide = this.handleNoteHide.bind(this);
    this.handlecRequestShow = this.handlecRequestShow.bind(this);
    this.handlecRequestHide = this.handlecRequestHide.bind(this);
    this.onRequestDelete = this.onRequestDelete.bind(this);
  }

  handlecRequestShow = (e) => {
    this.setState({
      cRequestShow: true
    });
    const id = e.target.id;
  }

  handlecRequestHide = () => {
    this.setState({
      cRequestShow: false
    });
  }


  onRequestDelete = (e) => {
      console.log('delete button clicked');
      // this.props.deleteApprovalStarted
    //  e.preventDefault();
    //  this.props.onRequestDelete(e);
    console.log(e.target.id);
  }

  handleContractShow = (e) => {

    this.setState({
      contractShow: true, 
      data: e.target.id
    });
    const id = e.target.id;
    console.log(id);
  }

  handleContractHide = () => {
    this.setState({
      contractShow: false
    });
  }

  handleNoteShow = (e) => {
    this.setState({
      noteShow: true
    });
    const id = e.target.id;
  }

  handleNoteHide = () => {
    this.setState({
      noteShow: false
    });
  }

  handleRearShow = (e) => {
    this.setState({
      rearShow: true
    });
    const id = e.target.id;
  }

  handleRearHide = () => {
    this.setState({
      rearShow: false
    });
  }

  handleFrontShow = (e) => {
    this.setState({
      frontShow: true
    });
    const id = e.target.id;
  }

  handleFrontHide = () => {
    this.setState({
      frontShow: false
    });
  }

  handleShow = (e) => {
    this.setState({
      modalShow: true
    });
    const id = e.target.id;
    console.log(e.target.id)
  }

  handleHide = () => {
    this.setState({
      modalShow: false
    })
  }

  onBeginChange = beginDate => this.setState({ beginDate })
  onEndChange = endDate => this.setState({ endDate })


  handlePageChange(pageNumber) {
    // axios.get('http://localhost:8000/api/approvals?page='+pageNumber)
    // .then(response => {
    //   this.setState({
    //     approvals: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //     })
    // })
  }

  componentDidMount = () => {
      this.props.getApprovalStarted();
      this.props.deleteApprovalStarted();

  }

  render() {
      const {errorMsg, rearShow, frontShow, modalShow, contractShow, noteShow, cRequestShow} = this.state;
      const approvalState = this.props.approvalState;
      const approvals = approvalState.data;
      const bodyStyle = {
        fontSize: "13px"
      }
      const divStyle = {
        fontSize: "14px"
      };
      const paperDesign = {
        borderBottomRightRadius: '20px',
        borderBottomLeftRadius: '20px'
      }

        return (
            <React.Fragment>
            <div>
              <h4 class="float-left">Даатгуулах хүсэлтийг хүлээж авсан иргэдийн жагсаалт</h4>
              <span className="float-right">
                <DatePicker
                  onChange={this.onBeginChange}
                  value={this.state.beginDate}
                />&nbsp;&nbsp;
                <DatePicker
                  onChange={this.onEndChange}
                  value={this.state.endDate}
                />&nbsp;&nbsp;&nbsp;
                <Button disabled variant="warning" size="sm">Экспортлох</Button>
              </span>
             </div><br /><br />
              <Paper style={paperDesign}>
                  <Table size="md" responsive hover>
                    <thead style={divStyle}>
                      <tr>
                        <th>#</th>
                        <th>Ургийн овог</th>
                        <th>Овог</th>
                        <th>Нэр</th>
                        <th>РД</th>
                        <th>Нас</th>
                        <th>Төрөл</th>
                        <th>Утас</th>
                        <th>Ир/үнэмлэх</th>
                        <th>Гэрээ хүлээн авах хаяг</th>
                        <th>Сонг/багц</th>
                        <th>Төл/төрөл</th>
                        <th>Хүс/Гар/Огноо</th>
                        <th className="d-flex justify-content-center">Багаж</th>
                      </tr>
                    </thead>  
                    <tbody style={bodyStyle}>
                      {(approvals.length > 0) ?
                        approvals.map(approval => (
                          <tr key={approval.id}>
                            <td>{approvals.indexOf(approval)+1}</td>  
                            <td>{approval.children_id = null ? approval.child_familyname : approval.familyname}</td>
                            <td>{approval.children_id = null ? approval.child_lastname : approval.lastname}</td>
                            <td>{approval.children_id = null ? approval.child_firstname : approval.firstname}</td>
                            <td>{approval.children_id = null ? approval.child_regnum : approval.regnum}</td>
                            <td>{approval.age}</td>
                            <td>
                              {approval.childred_id = null ? <MDBIcon icon="child"/> :
                              (approval.gender == 0 ? <MDBIcon icon="female" size="lg"/> : 
                              <MDBIcon icon="male" size="lg"/>)}
                            </td>
                            <td>{approval.phone_number}</td>
                            <td>
                              <ButtonToolbar>
                                <a href="#" onClick={this.handleFrontShow} id={approval.id}>Урд</a> &nbsp; 
                                <a href="#" onClick={this.handleRearShow} id={approval.id}>Ард</a>
                                <RearIdModal show={rearShow} onHide={this.handleRearHide} />
                                <FrontIdModal show={frontShow} onHide={this.handleFrontHide} />
                              </ButtonToolbar>
                            </td>
                            <td>{approval.contract_deliver_address}</td>
                            <td>{approval.set_name}</td>
                            <td>{approval.coupon_id = null ? 'Купон' : 'Мөнгө'} </td>
                            <td>{approval.created_at}</td>
                            <td>
                            <ButtonToolbar>
                              <EmailOutlinedIcon
                                onClick={this.handleShow}
                                id={approval.id}
                               />&nbsp;
                              <ContractOnWay show={modalShow} onHide={this.handleHide}/>

                              <AddCircleOutlineOutlinedIcon
                                onClick={(e) => this.handleContractShow(e)}
                                id={approval.id}
                                data={this.props.approvalState.data}
                               >+</AddCircleOutlineOutlinedIcon>&nbsp;
                              <Contract show={contractShow} onHide={this.handleContractHide}/>

                               <NoteAddOutlinedIcon
                                onClick={this.handleNoteShow}
                                id={approval.id}
                               >+</NoteAddOutlinedIcon>&nbsp;
                               <Notes show={noteShow} onHide={this.handleNoteHide}/>

                               <HighlightOffIcon
                                onClick={this.handlecRequestShow}
                                id={approval.id}
                                color="warning"
                               >+</HighlightOffIcon>&nbsp;
                               <CancelRequest show={cRequestShow} onHide={this.handlecRequestHide} onDelete={this.onRequestDelete}/>
                            </ButtonToolbar>
                            </td>
                          </tr>
                          )) : ''
                        }
                      </tbody>
                    </Table> 
              </Paper><br/>
              <div className="d-flex justify-content-center">
                <Pagination 
                  activePage = {approvalState.current_page}
                  itemsCountPerPage = {approvalState.per_page}
                  totalItemsCount = {approvalState.total}
                  pageRangeDisplayed = {this.state.pageRangeDisplayed}
                  onChange = {this.handlePageChange}
                  itemClass='page-item'
                  linkClass='page-link'
                />
              </div>
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getApprovalStarted,
    deleteApprovalStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        approvalState: state.Approval
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Approved);