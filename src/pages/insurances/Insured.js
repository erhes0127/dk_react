import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { getLoggedInUser } from '../../helpers/authUtils';
// import Loader from '../../components/Loader';
import { Row, Col, Card, CardBody } from 'reactstrap';
import Form from 'react-bootstrap/Form';
// import { makeStyles } from '@material-ui/core/styles';
// import Table from '@material-ui/core/Table';
import Table from 'react-bootstrap/Table';
// import TableBody from '@material-ui/core/TableBody';
// import TableCell from '@material-ui/core/TableCell';
// import TableHead from '@material-ui/core/TableHead';
// import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import { Button, ButtonToolbar } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
// import ModalHeader from 'react-bootstrap/ModalHeader';
// import ModalTitle from 'react-bootstrap/ModalTitle';
// import ModalBody from 'react-bootstrap/ModalBody';
// import ModalFooter from 'react-bootstrap/ModalFooter';
// import ModalDialog from 'react-bootstrap/ModalDialog';
import DatePicker from 'react-date-picker';
import "react-datepicker/dist/react-datepicker.css";
import { bindActionCreators } from 'redux';
import { getInsuredStarted } from '../../redux/insured/actions';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { MDBIcon, MDBBtn } from "mdbreact";
import NumberFormat from 'react-number-format';

function InsuredModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Хувийн мэдээлэл
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
              <Row xs="2">
                <Col>
                  <Form.Group>
                    <Form.Label>Ургийн овог</Form.Label>
                    <Form.Control type="text" placeholder="{insurance.familyname}" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Нэр</Form.Label>
                    <Form.Control type="text" placeholder="Нэр" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Дугаар</Form.Label>
                    <Form.Control type="number" placeholder="Дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Цахим шуудан</Form.Label>
                    <Form.Control type="text" placeholder="Цахим шуудан" value="" disabled/>
                  </Form.Group>
                   <Form.Group>
                    <Form.Label>Гэрийн хаяг</Form.Label>
                    <Form.Control type="text" placeholder="Гэрийн хаяг" value="" disabled/>
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group>
                    <Form.Label>Овог</Form.Label>
                    <Form.Control type="text" placeholder="Овог" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Регистрийн дугаар</Form.Label>
                    <Form.Control type="text" placeholder="Регистрийн дугаар" value="" disabled/>
                  </Form.Group>
                  <Form.Group>
                    <Form.Label>Холбоо барих дугаар</Form.Label>
                    <Form.Control type="number" placeholder="Холбоо барих дугаар" value="" disabled/>
                  </Form.Group>
                </Col>
              </Row>
          </Form>
        </Modal.Body>
      </Modal>
    );
}

function FrontIdModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Иргэний үнэмлэхний урд талын зураг
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p>front id photo</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}

function RearIdModal(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Иргэний үнэмлэхний хойд талын зураг
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p>rear id photo</p>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}
function Payment(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Төлбөр төлөлт
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Table>
              <thead>
                  <tr>
                    <th>#</th>
                    <th>Төлсөн дүн</th>
                    <th>Төлсөн сар</th>
                    <th>Төлөлт эхлэх огноо</th>
                    <th>Төлөлт дуусах огноо</th>
                    <th>Төлөгдсөн огноо</th>
                    <th>Төлөлт хоцорсон хоног</th>
                    <th>Төрөл</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>  
                    <th>#</th>
                    <th>*</th>
                    <th>*</th>
                    <th>*</th>
                    <th>*</th>
                    <th>*</th>
                    <th>*</th>
                    <th>*</th>
                  </tr>
              </tbody>
            </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={props.onHide}>Хаах</Button>
        </Modal.Footer>
      </Modal>
    );
}

class Insured extends Component {

  constructor(props) {
    super(props)
    this.state = {
      insurance: [],
      errorMsg: '',
      title: '',
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3,
      modalShow: false,
      beginDate: new Date(),
      endDate: new Date(),
      paymentShow: false,

    }
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleShow       = this.handleShow.bind(this);
    this.handleHide       = this.handleHide.bind(this);
    this.handleRearShow   = this.handleRearShow.bind(this);
    this.handleRearHide   = this.handleRearHide.bind(this);
    this.handleFrontShow  = this.handleFrontShow.bind(this);
    this.handleFrontHide  = this.handleFrontHide.bind(this);
    this.paymentShow      = this.paymentShow.bind(this);
    this.paymentHide      = this.paymentHide.bind(this);
  }

  paymentShow = (e) => {
    this.setState({
      paymentShow: true
    });
    const id = e.target.id;
  }

  paymentHide = () => { this.setState({ paymentShow: false }) }

  handleRearShow = (e) => {
    this.setState({
      rearShow: true
    });
    const id = e.target.id;
  }

  handleRearHide = () => { this.setState({ rearShow: false }) }

  handleFrontShow = (e) => {
    this.setState({
      frontShow: true,
      // data: this.props.insuredState.data
    });
    const id = e.target.id;
  }

  handleFrontHide = () => { this.setState({ frontShow: false }) }

  handleShow = (e) => {
    this.setState({
      modalShow: true
    });
    const id = e.target.id;
  }

  handleHide = () => { this.setState({ modalShow: false }) }

  onBeginChange = beginDate => this.setState({ beginDate })
  onEndChange = endDate => this.setState({ endDate })

  handlePageChange(pageNumber) {
    // axios.get('http://localhost:8000/api/insurance?page='+pageNumber)
    // .then(response => {
    //   this.setState({
    //     insurance: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //     })
    // })
    console.log(`active page is ${pageNumber}`);
    console.log(this.props.insuredState.data);
    this.setState({
      activePage: pageNumber
    });

  }

  // componentDidMount() {
    // axios.get('http://localhost:8000/api/insurance')
    // .then(response => {
    //   this.setState({
    //     insurance: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //   })
    // })
    // .catch(error => {
    //   console.log(error)
    //   this.setState({errorMsg: 'Error retreiving data '})
    // })
  // }

 
  

    render() {
      const {modalShow, rearShow, frontShow, paymentShow} = this.state;
      const insuredState = this.props.insuredState;
      const insurance = insuredState.data;
      const divStyle = {
        fontSize: "14px"
      }
      const bodyStyle = {
        fontSize: "13px"
      }
      const paperDesign = {
        borderBottomRightRadius: '20px',
        borderBottomLeftRadius: '20px'
      }

        return (
            <React.Fragment>
            <div>
              <h4 class="float-left">Даатгуулах хүсэлтийг хүлээж авсан иргэдийн жагсаалт</h4>
              <span className="float-right">
                <DatePicker
                    onChange={this.onBeginChange}
                    value={this.state.beginDate}
                />&nbsp;&nbsp;
                <DatePicker
                  onChange={this.onEndChange}
                  value={this.state.endDate}
                />&nbsp;&nbsp;
                <Button variant="warning" size="sm">Экспортлох</Button>
              </span>
             </div><br /><br />
              <Paper style={paperDesign}>
                <Table size="md" responsive hover>
                  <thead style={divStyle}>
                    <tr size="sm">
                      <th>#</th>
                      <th>Ургийн овог</th>
                      <th>Овог</th>
                      <th>Нэр</th>
                      <th>Регистр</th>
                      <th>Нас</th>
                      <th>Төрөл</th>
                      <th>Утас</th>
                      <th>Ир/үнэмлэх</th>
                      <th>Гэр/дугаар</th>
                      <th>Сонг/Багц</th>
                      <th>Төл/Төрөл</th>
                      <th>Төлөлт</th>
                      <th>Нөх/олговор</th>
                      <th>ГБО</th>
                      <th>ГДО</th>
                      <th>ДСТ</th>
                      <th>-</th>
                    </tr>
                  </thead>
                  <tbody style={bodyStyle}>
                  {(insurance.length > 0) ?
                    insurance.map(insured => (
                      <tr key={insured.id}>  
                        <td>{insurance.indexOf(insured)+1}</td>
                        <td>{insured.child_familyname = null ? insured.child_familyname : insured.familyname}</td>
                        <td>{insured.child_lastname = null ? insured.child_lastname : insured.lastname}</td>
                        <td>{insured.child_firstname = null ? insured.child_firstname : insured.firstname}</td>
                        <td>{insured.child_regnum = null ? insured.child_regnum : insured.regnum}</td>
                        <td>{insured.age}</td>
                        <td>
                            {insured.childred_id = null ? <MDBIcon icon="child"/> :
                            (insured.gender == 0 ? <MDBIcon icon="female" size="lg"/> : 
                            <MDBIcon icon="male" size="lg"/>)}
                        </td>
                        <td>{insured.phonenumber}</td>
                        <td>
                              <ButtonToolbar>
                                <a href="#" onClick={this.handleFrontShow} id={insured.id}>Урд</a> &nbsp; 
                                <a href="#" onClick={this.handleRearShow} id={insured.id}>Ард</a>
                                <RearIdModal show={rearShow} onHide={this.handleRearHide} />
                                <FrontIdModal show={frontShow} onHide={this.handleFrontHide} />
                              </ButtonToolbar>
                        </td>
                        <td>{insured.contract_number}</td>
                        <td>{insured.set_name}</td>
                        <td>{insured.coupon_id = null ? 'Купон' : 'Мөнгө'}</td>
                        <td>?сар</td>
                        <td><a href="/claims">
                        <NumberFormat value={insured.claim} displayType={'text'} thousandSeparator={true} prefix={'₮'} />
                        </a></td>
                        <td>{insured.contract_created_at}</td>
                        <td>{insured.contract_expires_at}</td>
                        <td><a href="#" onClick={this.paymentShow} id={insured.id}>{insured.payment_deadline}</a></td>
                            <Payment show={paymentShow} onHide={this.paymentHide} />
                        <td>
                          <ButtonToolbar>
                              <VisibilityIcon
                               onClick={this.handleShow}
                               id={insured.id}
                               size="sm"
                               color="warning"
                               >+</VisibilityIcon>&nbsp;
                              <InsuredModal show={modalShow} onHide={this.handleHide}/>
                          </ButtonToolbar>
                        </td>
                      </tr>
                   )) : ''
                  }
                  </tbody>
                </Table>
              </Paper> <br />

              <div class="d-flex justify-content-center">
                <Pagination 
                  activePage = {insuredState.activePage}
                  itemsCountPerPage = {insuredState.itemsCountPerPage}
                  totalItemsCount = {insuredState.total}
                  pageRangeDisplayed = {this.state.pageRangeDisplayed}
                  onChange = {this.handlePageChange}
                  itemClass = 'page-item'rear id photo
                  linkClass = 'page-link'
                />
              </div> <br />
                
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getInsuredStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        insuredState: state.Insured
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Insured);
