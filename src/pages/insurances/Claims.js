import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col, Card, CardBody } from 'reactstrap';
import { getLoggedInUser } from '../../helpers/authUtils';
import Loader from '../../components/Loader';
import Table from 'react-bootstrap/Table';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import { bindActionCreators } from 'redux';
import { Button, ButtonToolbar } from 'react-bootstrap';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';

class Claims extends Component {

  constructor(props) {
    super(props)
    this.state = {
      errorMsg: '',
      title: '',

    }
  }

  

    render() {
      const {errorMsg} = this.state;

      const divStyle = {
        fontSize: "14px"
      };
      const bodyStyle = {
        fontSize: "13px"
      }
        return (
            <React.Fragment>
              <h4>НӨХӨН ОЛГОВОРЫН МЭДЭЭЛЭЛ</h4><br /> <br />
              <Paper>
                <Row xs="2">
                  <Col>
                      <h5 className="float-left">Мэс засал</h5>
                      <AddCircleOutlineOutlinedIcon className="float-right" variant="secondary" size="sm" /><br />
                      <Table size="md" responsive hover>
                      <thead style={divStyle}>
                          <tr>
                          <th>#</th>
                          <th>НООА</th>
                          <th>НО</th>
                          <th>Тэмдэглэл</th>
                          <th>Төлөв</th>
                          <th>Огноо</th>
                          </tr>
                      </thead>
                      <tbody style={bodyStyle}>
                              <tr>  
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              </tr>
                      </tbody>
                      <p>&nbsp;&nbsp;Нийт: 0, Хязгаар: 1,000,000, Үлдэгдэл: 1,000,000</p>
                      </Table>
                  </Col>
                  <Col>
                      <h5 className="float-left">Гэнэтийн осол</h5>
                      <AddCircleOutlineOutlinedIcon className="float-right" variant="secondary" size="sm" />
                     <Table size="md" responsive hover>
                      <thead style={divStyle}>
                          <tr>
                          <th>#</th>
                          <th>НООА</th>
                          <th>НО</th>
                          <th>Тэмдэглэл</th>
                          <th>Төлөв</th>
                          <th>Огноо</th>
                          </tr>
                      </thead>
                      <tbody style={bodyStyle}>
                              <tr>  
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              </tr>
                      </tbody>
                      <p>&nbsp;&nbsp;Нийт: 0, Хязгаар: 1,000,000, Үлдэгдэл: 1,000,000</p>
                      </Table>
                  </Col>
                </Row>
              </Paper><br /> <br /><br />
              <Paper>
                <Row xs="2">
                  <Col>
                      <h5 className="float-left">Амбулатори</h5>
                      <AddCircleOutlineOutlinedIcon className="float-right" variant="secondary" size="sm" />
                      <Table size="md" responsive hover>
                      <thead style={divStyle}>
                          <tr>
                          <th>#</th>
                          <th>НООА</th>
                          <th>НО</th>
                          <th>Тэмдэглэл</th>
                          <th>Төлөв</th>
                          <th>Огноо</th>
                          </tr>
                      </thead>
                      <tbody style={bodyStyle}>
                              <tr>  
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              </tr>
                      </tbody>
                      <p>&nbsp;&nbsp;Нийт: 0, Хязгаар: 1,000,000, Үлдэгдэл: 1,000,000</p>
                      </Table>
                  </Col>
                  <Col>
                      <h5 className="float-left">Хэвтэн эмчлүүлэх</h5>
                      <AddCircleOutlineOutlinedIcon className="float-right" variant="secondary" size="sm" />
                     <Table size="md" responsive hover>
                      <thead style={divStyle}>
                          <tr>
                          <th>#</th>
                          <th>НООА</th>
                          <th>НО</th>
                          <th>Тэмдэглэл</th>
                          <th>Төлөв</th>
                          <th>Огноо</th>
                          </tr>
                      </thead>
                      <tbody style={bodyStyle}>
                              <tr>  
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              <td>*</td>
                              </tr>
                      </tbody>
                      <p>&nbsp;&nbsp;Нийт: 0, Хязгаар: 1,000,000, Үлдэгдэл: 1,000,000</p>
                      </Table>
                  </Col>
                </Row>
              </Paper>
              
                
            </React.Fragment>
        )
    }
}

export default connect()(Claims);
