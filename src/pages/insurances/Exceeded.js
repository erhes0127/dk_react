import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { Row, Col, Card, CardBody } from 'reactstrap';
import { getLoggedInUser } from '../../helpers/authUtils';
// import Loader from '../../components/Loader';
import Table from 'react-bootstrap/Table';
import Modal from 'react-bootstrap/Modal';
import ModalHeader from 'react-bootstrap/ModalHeader';
import ModalTitle from 'react-bootstrap/ModalTitle';
import ModalBody from 'react-bootstrap/ModalBody';
import ModalFooter from 'react-bootstrap/ModalFooter';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import Pagination from 'react-js-pagination';
import { bindActionCreators } from 'redux';
import { getExceedStarted } from '../../redux/exceed/actions';
import DatePicker from 'react-date-picker';
import { Button, ButtonToolbar } from 'react-bootstrap';
import NoteAddOutlinedIcon from '@material-ui/icons/NoteAddOutlined';
import Moment from 'react-moment';
import 'moment-timezone';
import AddCircleOutlineOutlinedIcon from '@material-ui/icons/AddCircleOutlineOutlined';
import NumberFormat from 'react-number-format';

function Note(props) {
    return (
      <Modal
        {...props}
        size="mb-3"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Тэмдэглэл оруулах
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <p className="float-left">Хэрэглэгч: firstname(phone_number)</p>
            <AddCircleOutlineOutlinedIcon variant="warning" className="float-right"/>
            <br /> <hr />
            <p>Тэмдэглэлийн жагсаалт</p>
            <ul>
              <li>Хоосон байна</li>
            </ul>
        </Modal.Body>
      </Modal>
    );
}

class Exceeded extends Component {

  constructor(props) {
    super(props)
    this.state = {
      exceed: [],
      errorMsg: '',
      title: '',
      activePage:1,
      itemsCountPerPage: 1,
      totalItemsCount: 1,
      pageRangeDisplayed: 3,
      noteShow: false,

    }
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleNoteShow = this.handleNoteShow.bind(this);
    this.handleNoteHide = this.handleNoteHide.bind(this);

  }

  handleNoteShow = (e) => {
    this.setState({
      noteShow: true
    });
    const id = e.target.id;
  }

  handleNoteHide = () => {
    this.setState({
      noteShow: false
    });
  }

  handlePageChange(pageNumber) {
    axios.get('http://localhost:8000/api/exceed?page='+pageNumber)
    .then(response => {
      this.setState({
        exceedState: response.data.data,
        itemsCountPerPage: response.data.per_page,
        totalItemsCount: response.data.total,
        activePage: response.data.current_page
        })
    })
  }

  onBeginChange = beginDate => this.setState({ beginDate })
  onEndChange = endDate => this.setState({ endDate })

  componentDidMount() {
    // axios.get('http://localhost:8000/api/exceed')
    // .then(response => {
    //   console.log(response)
    //   this.setState({
    //     exceed: response.data.data,
    //     itemsCountPerPage: response.data.per_page,
    //     totalItemsCount: response.data.total,
    //     activePage: response.data.current_page
    //   })
    // })
    // .catch(error => {
    //   console.log(error)
    //   this.setState({errorMsg: 'Error retreiving data '})
    // })
  }

  

    render() {
      const {errorMsg, noteShow} = this.state;
      const exceedState = this.props.exceedState;
      const exceed = exceedState.data;
      // const now = moment(new Date());
      // console.log(now);
      const divStyle = {
        fontSize: "14px"
      };
      const bodyStyle = {
        fontSize: "13px"
      }
      const textColor = {
        color: "red"
      }
      const paperDesign = {
        borderBottomRightRadius: '20px',
        borderBottomLeftRadius: '20px'
      }
        return (
            <React.Fragment>
            <div>
              <h4 class="float-left">Төлбөр төлөх хугацаа хэтэрсэн даатгалтай иргэд</h4>
              <span className="float-right">
                <DatePicker
                  onChange={this.onBeginChange}
                  value={this.state.beginDate}
                />&nbsp;&nbsp;
                <DatePicker
                  onChange={this.onEndChange}
                  value={this.state.endDate}
                />&nbsp;&nbsp;
                <Button variant="warning" size="sm">Экспортлох</Button>
              </span>
             </div><br /><br />
              <Paper style={paperDesign}>
                <Table size="md" responsive hover>
                  <thead style={divStyle}>
                    <tr>
                      <th>#</th>
                      <th>Ургийн овог</th>
                      <th>Овог</th>
                      <th>Нэр</th>
                      <th>РД</th>
                      <th>Утас</th>
                      <th>Гэр/Дугаар</th>
                      <th>Сонг/Багц</th>
                      <th>Нөхөн олговор</th>
                      <th>ГБО</th>
                      <th>ГДО</th>
                      <th>ДСТ</th>
                      <th>Хэт/Хон</th>
                      <th>-</th>
                    </tr>
                  </thead>
                  <tbody style={bodyStyle}>
                  {(exceed.length > 0) ?
                    exceed.map(exceeded => (
                        <tr key={exceeded.id}>  
                          <td>{exceed.indexOf(exceeded)+1}</td>
                          <td>{exceeded.children_id = null ? exceeded.children_familyname : exceeded.familyname}</td>
                          <td>{exceeded.children_id = null ? exceeded.children_lastname : exceeded.lastname}</td>
                          <td>{exceeded.children_id = null ? exceeded.children_firstname : exceeded.firstname}</td>
                          <td>{exceeded.children_id = null ? exceeded.children_regnum : exceeded.regnum}</td>
                          <td>{exceeded.phone_number}</td>
                          <td>{exceeded.contract_number}</td>
                          <td>{exceeded.set_name}</td>
                          <td><a href="/claims">
                          <NumberFormat value={exceeded.cover_balance} displayType={'text'} thousandSeparator={true} prefix={'₮'} />
                          </a></td>
                          <td>{exceeded.contract_created_at}</td>
                          <td>{exceeded.contract_expires_at}</td>
                          <td>{exceeded.payment_deadline}</td>
                          <td style={textColor}><Moment fromNow format="DD">{exceeded.payment_deadline}</Moment></td>
                          <td>
                          <ButtonToolbar>
                             <NoteAddOutlinedIcon
                               onClick={this.handleNoteShow}
                               id={exceeded.id}
                               size="sm"
                               color="warning"
                               >+</NoteAddOutlinedIcon>&nbsp;
                              <Note show={noteShow} onHide={this.handleNoteHide}/>
                          </ButtonToolbar>
                          </td>
                        </tr>
                    )) : ''
                   }
                  </tbody>
                </Table>
              </Paper> <br/>

              <div class="d-flex justify-content-center">
                <Pagination 
                  activePage = {exceedState.current_page}
                  itemsCountPerPage={exceedState.per_page}
                  totalItemsCount={exceedState.total}
                  pageRangeDisplayed={this.state.pageRangeDisplayed}
                  onChange={this.handlePageChange}
                  itemClass='page-item'
                  linkClass='page-link'
                />
              </div> <br />
                
            </React.Fragment>
        )
    }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    getExceedStarted,
  }, dispatch)
}

const mapStateToProps = state => {
    return {
        exceedState: state.Exceed
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Exceeded);



